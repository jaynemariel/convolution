#include <cstdlib>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "verified_quad.h"

// Performs Extended Euclidean Algorithm
// Used for computing multiplicative inverses mod p
void extEuclideanAlg(Elem u, Elem *u1, Elem *u2, Elem *u3) {
  *u1 = 1;
  *u2 = 0;
  *u3 = u;
  Elem v1 = 0;
  Elem v2 = 1;
  Elem v3 = PRIME;
  Elem q;
  Elem t1;
  Elem t2;
  Elem t3;
  do {
    q = *u3 / v3;
    // t1 = *u1 + p - q * v1;
    // t2 = *u2 + p - q*v2;
    // t3 = *u3 + p - q*v3;
    t1 = mod((*u1) + PRIME - mod_mlt(q, v1));
    t2 = mod((*u2) + PRIME - mod_mlt(q, v2));
    t3 = mod((*u3) + PRIME - mod_mlt(q, v3));
    (*u1) = v1;
    (*u2) = v2;
    (*u3) = v3;
    v1 = t1;
    v2 = t2;
    v3 = t3;
  } while (v3 != 0 && v3 != PRIME);
}

// Computes the modular multiplicative inverse of a modulo m,
// using the extended Euclidean algorithm
// only works for p=2^61-1
Elem inv(Elem a) {
  Elem u1;
  Elem u2;
  Elem u3;
  extEuclideanAlg(a, &u1, &u2, &u3);
  if (u3 == 1)
    return mod(u1);
  else
    return 0;
}

// extrapolate the polynomial implied by vector vec of length n to location r
Elem extrap(Elem vec[], Elem n, Elem r) {
  Elem result = 0;
  Elem mult = 1;
  for (Elem i = 0; i < n; i++) {
    mult = 1;
    for (Elem j = 0; j < n; j++) {
      if (i > j)
        mult = mod_mlt(mod_mlt(mult, mod(r - j + PRIME)), inv(i - j));
      if (i < j)
        mult =
            mod_mlt(mod_mlt(mult, mod(r - j + PRIME)), inv(mod(i + PRIME - j)));
    }
    result = mod(result + mod_mlt(mult, vec[i]));
  }
  return result;
}

// evaluate the MLE of I at q and random location r in O(logn)
Elem evaluate_I(Elem q[], Elem r[], int d) {
  Elem ans = 1;
  for (Elem k = 0; k < d; k++)
    ans = mod_mlt(ans, mod(mod_mlt(q[k], r[k]) +
                           mod_mlt(1 + PRIME - q[k], 1 + PRIME - r[k])));
  return ans;
}

// evaluates V_i polynomial at location r.
// O(n)
Elem evaluate_V_i(int mi, int ni, Elem level_i[], Elem r[]) {
  Elem *chi_eval = (Elem *)calloc(ni, sizeof(Elem));
  Elem num_terms = 1;
  chi_eval[0] = 1;
  Elem temp;

  for (int i = 0; i < mi; i++) {
    for (int j = 0; j < num_terms; j++) {
      temp = chi_eval[j];
      chi_eval[j] = mod_mlt(temp, 1 + PRIME - r[i]);
      chi_eval[j + num_terms] = mod_mlt(temp, r[i]);
    }
    num_terms = num_terms << 1;
  }
  Elem ans = 0;
  for (Elem k = 0; k < ni; k++)
    ans = mod(ans + mod_mlt(level_i[k], chi_eval[k]));
  free(chi_eval);
  return ans;
}

void check_rounds_quad(Elem a1, Elem a2, Elem F[][4], Elem r[], Elem d,
                       Elem g) {
  if (a1 != mod(F[0][0] + F[0][1]))
    std::cout << "....first check failed" << std::endl, exit(1);
  Elem check;
  for (int i = 1; i < d; i++) {
    check = extrap(F[i - 1], g, r[i - 1]);
    if ((mod(F[i][0] + F[i][1]) != check) &&
        (mod(F[i][0] + F[i][1]) + PRIME != check))
      std::cout << "check " << i << " failed" << std::endl, exit(1);
  }

  check = extrap(F[d - 1], g, r[d - 1]);
  if (a2 != check)
    std::cout << "last check failed" << std::endl, exit(1);
}

int main() {

  //const int LOG_DIM = 22;
  //const int DIM = 4194304; // 2^22 = 4194304

  // input values
  static Elem V[DIM];
  static Elem Vcopy[DIM];
  for (int i = 0; i < DIM; i++) {
    V[i] = rand() % 100;
    Vcopy[i] = V[i];
  }

  //// I values
  //static Elem I[DIM];
  //for (int i = 0; i < DIM; i++)
  //  I[i] = 1;

  // output of quadratic activation
  static Elem A[DIM];

  // sum-check protocol random values for each iteration
  // should be picked from the field [0, F_p], but this picks from [0, RAND_MAX]
  static Elem r[LOG_DIM];
  for (int i = 0; i < LOG_DIM; i++)
    r[i] = rand();

  // random point q to evaluate the output matrix C
  static Elem q[LOG_DIM];
  for (int i = 0; i < LOG_DIM; i++)
    q[i] = rand();

  // polynomial extrapolations for each round
  static Elem F[LOG_DIM][4];

  clock_t t = clock();
  // local computation using field arithmetic
  for (int i = 0; i < DIM; i++)
    A[i] = mod_mlt(V[i], V[i]);
  t = clock() - t;
  std::cout << "unverifiable time = " << (double)t / CLOCKS_PER_SEC
            << std::endl;

  // local computation without using field arithmetic
  for (int i = 0; i < DIM; i++)
    A[i] = (V[i] * V[i]);
  t = clock() - t;
  std::cout << "unverifiable time w/o field arith = "
            << (double)t / CLOCKS_PER_SEC << std::endl;

  // evalute a random point in the MLE of the returned matrix
  // for the output layer this is done by the verifier, for intermediate layers
  // during sum-check by prover
  t = clock();
  Elem a1 = evaluate_V_i(LOG_DIM, DIM, A, q);
  t = clock() - t;
  std::cout << "V1 (out) time = " << (double)t / CLOCKS_PER_SEC << std::endl;

  // For intermediate layers, this claim is returned by the prover. For input,
  // the verifier computes this
  Elem Veval;

  // prover's work for the sum-check protocol
  int proveIt = 1;
  int rounds = 1;
  t = clock();
  //sum_check_activation(proveIt, q, r, LOG_DIM, DIM, I, V, F, &Veval);
  //for (int rounds = 0 ; rounds < 2 ; rounds++){
    sum_check_activation_hw(proveIt, rounds, q, r, V, F);
  //}
  t = clock() - t;
  std::cout << "additional P time = " << (double)t / CLOCKS_PER_SEC
            << std::endl;

  // verifier checks the assertion on the MLE of the input
  // evaluate V at r
  t = clock();
  Veval = evaluate_V_i(LOG_DIM, DIM, Vcopy, r);
  t = clock() - t;
  std::cout << "V2 (in) time = " << (double)t / CLOCKS_PER_SEC << std::endl;

  // verifier checks each iteration of the sum-check protocol
  t = clock();
  // evaluate I at (q,r)
  Elem Ieval = evaluate_I(q, r, LOG_DIM);
  Elem a2 = mod_mlt(mod_mlt(Veval, Veval), Ieval);

  check_rounds_quad(a1, a2, F, r, LOG_DIM, 4);

  t = clock() - t;
  std::cout << "verifier time = " << (double)t / CLOCKS_PER_SEC << std::endl;

  // print_values(a1, a2, F, r, LOG_DIM, 4);

  std::cout << "\nPASSED: computation verified!\n" << std::endl;

  return 0;
}
