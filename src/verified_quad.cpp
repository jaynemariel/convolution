#include "verified_quad.h"

// Protocol reduces verifying a claim that v_i-1(q)=a_i-1 to verifying that
// v_i(q')=a_i
Elem sum_check_activation_hw(int proveIt,int rounds,  Elem q[], Elem r[], Elem V[],
                          Elem F[][4]) {

  //int d = LOG_DIM;
  //int n = DIM;


  // I values
  static Elem I[DIM];
  for (int i = 0; i < DIM; i++)
    I[i] = 1;

  // initialize I values optimized method
  Elem I_tmp;
  Elem steps = 1;
  for (int i = 0; i < LOG_DIM; i++) {
    for (int k = 0; k < steps; k++) {
      I_tmp = I[k];

      I[k] = mod_mlt(I_tmp, 1 + PRIME - q[i]);
      I[k + steps] = mod_mlt(I_tmp, q[i]);
    }

    steps = steps << 1;
  }

  // partial sums for calculating F at each round
  Elem parsumV[4];
  Elem parsumI[4];

  steps = DIM;
  int j;

  for (int i = 0; i < LOG_DIM; i++) { // rounds of the protocol
    steps = steps >> 1;
    for (int k = 0; k < steps; k++) {
      j = 2 * k;
      parsumV[0] = V[j];
      parsumV[1] = V[j + 1];
      parsumV[2] = mod(2 * V[j + 1] + PRIME - V[j]);
      parsumV[3] = mod(3 * V[j + 1] + 2 * (PRIME - V[j]));

      parsumI[0] = I[j];
      parsumI[1] = I[j + 1];
      parsumI[2] = mod(2 * I[j + 1] + PRIME - I[j]);
      parsumI[3] = mod(3 * I[j + 1] + 2 * (PRIME - I[j]));

      // check using update_V
      V[k] = mod(mod_mlt(V[j], 1 + PRIME - r[i]) + mod_mlt(V[j + 1], r[i]));
      I[k] = mod(mod_mlt(I[j], 1 + PRIME - r[i]) + mod_mlt(I[j + 1], r[i]));

      for (int m = 0; m < 4; m++)
        F[i][m] =
            mod(F[i][m] + mod_mlt(mod_mlt(parsumV[m], parsumV[m]), parsumI[m]));
    }
  }
  //*Veval = V[0];
}
