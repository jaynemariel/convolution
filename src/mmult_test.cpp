#include <stdio.h>
#include <stdlib.h>

#include "mmult.h"

// MM reference implementation.
void mmult_sw_reference(Elem a[INPUT_DIM][INPUT_DIM],
                        Elem b[INPUT_DIM][INPUT_DIM],
                        Elem out[INPUT_DIM][INPUT_DIM]) {
  // Matrix multiplication of a A*B matrix.
  for (int ia = 0; ia < INPUT_DIM; ++ia) {
    for (int ib = 0; ib < INPUT_DIM; ++ib) {
      Elem sum = 0;
      for (int id = 0; id < INPUT_DIM; ++id) {
        sum += a[ia][id] * b[id][ib];
      }
      out[ia][ib] = sum;
    }
  }
}

int test_standalone(void) {
  static Elem matOp1[INPUT_DIM][INPUT_DIM];
  static Elem matOp2[INPUT_DIM][INPUT_DIM];
  static Elem matMult_sw[INPUT_DIM][INPUT_DIM];
  static Elem matMult_hw[INPUT_DIM][INPUT_DIM];

  // Initialize matrices.
  int i, j;
  for (i = 0; i < INPUT_DIM; i++)
    for (j = 0; j < INPUT_DIM; j++)
      matOp1[i][j] = (Elem)(i + j);
  for (i = 0; i < INPUT_DIM; i++)
    for (j = 0; j < INPUT_DIM; j++)
      matOp2[i][j] = (Elem)(i * j);

  mmult_hw(matOp1, matOp2, matMult_hw);

  // Run reference implementation.
  mmult_sw_reference(matOp1, matOp2, matMult_sw);

  // Compare results.
  int err = 0;
  for (i = 0; (i < INPUT_DIM && !err); i++)
    for (j = 0; (j < INPUT_DIM && !err); j++)
      if (matMult_sw[i][j] != matMult_hw[i][j])
        err++;

  return err;
}

int test_wrapped(void) {
  static Elem matOp1[INPUT_DIM][INPUT_DIM];
  static Elem matOp2[INPUT_DIM][INPUT_DIM];
  static Elem matMult_sw[INPUT_DIM][INPUT_DIM];
  static Elem matMult_hw[INPUT_DIM][INPUT_DIM];

  int i, j, err;

  /** Matrix Initiation */
  for (i = 0; i < INPUT_DIM; i++)
    for (j = 0; j < INPUT_DIM; j++)
      matOp1[i][j] = (Elem)(i + j);

  for (i = 0; i < INPUT_DIM; i++)
    for (j = 0; j < INPUT_DIM; j++)
      matOp2[i][j] = (Elem)(i * j);
  /** End of Initiation */

  // Prepare data for the DUT.
  AxiPacket inp_stream[2 * INPUT_DIM * INPUT_DIM / ELEMENT_PER_PACKET];
  AxiPacket out_stream[INPUT_DIM * INPUT_DIM / ELEMENT_PER_PACKET];

  // Input and output stream indices.
  int is_idx = 0;
  int os_idx = 0;

  AxiPacket packet;

  // Stream in the first input matrix.
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j += ELEMENT_PER_PACKET) {
      for (int k = 0; k < ELEMENT_PER_PACKET; k++) {
        packet.data[k] = matOp1[i][j + k];
      }
      inp_stream[is_idx++] = packet;
    }
  }

  // Stream in the second input matrix.
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j += ELEMENT_PER_PACKET) {
      for (int k = 0; k < ELEMENT_PER_PACKET; k++) {
        packet.data[k] = matOp2[i][j + k];
      }
      inp_stream[is_idx++] = packet;
    }
  }

  // Call the DUT.
  mmult_hw_wrapped(inp_stream, out_stream);

  // Extract the output matrix from the out stream.
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j += ELEMENT_PER_PACKET) {
      packet = out_stream[os_idx++];
      for (int k = 0; k < ELEMENT_PER_PACKET; k++) {
        matMult_hw[i][j + k] = packet.data[k];
      }
    }
  }

  // Run reference Matrix Multiplication.
  mmult_sw_reference(matOp1, matOp2, matMult_sw);

  // Compare matrices.
  err = 0;
  for (i = 0; (i < INPUT_DIM && !err); i++)
    for (j = 0; (j < INPUT_DIM && !err); j++)
      if (matMult_sw[i][j] != matMult_hw[i][j])
        err++;

  return err;
}

int main(void) {
  printf("\n=== Matrix multiplication test\n");
  printf("WRAPPED: %d\n", WRAPPED);
  printf("INPUT_DIM: %d\n", INPUT_DIM);
  printf("ELEMENT_PER_PACKET: %d\n", ELEMENT_PER_PACKET);

#if WRAPPED == 0
  int err = test_standalone();
#else
  int err = test_wrapped();
#endif

  if (err == 0) {
    printf("==> TEST PASSED: the reference matrix multiplication output the "
           "same matrix.\n");
  } else {
    printf("!!!!!! FAILED !!!!!!\n");
  }

  return err;
}
