#include <ap_axi_sdata.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////
// PARAMETERS.
//////////////////////////////////////////////////////////////////////

// Strides.
#define STRIDE 1

///////////////////// Test constants ///////////////////////////
#ifdef QUICK_TEST

// Input activation tensor dimension (W1).
//#define INPUT_DIM 32
//#define LOG_INPUT_DIM 5
#define INPUT_DIM 8
#define LOG_INPUT_DIM 3

// Number of channels (D1).
#define N_CHAN 2
#define LOG_N_CHAN 1

// Number of filters / depth of output (D2).
//#define N_FILT 16
//#define LOG_N_FILT 4
#define N_FILT 2
#define LOG_N_FILT 1

// Filter dimension (K).
//#define FILTER_DIM 8
//#define LOG_FILTER_DIM 3
#define FILTER_DIM 4
#define LOG_FILTER_DIM 2

// Number of batches (B).
//#define N_BATCH 32
//#define LOG_N_BATCH 5
#define N_BATCH 1
#define LOG_N_BATCH 0

// Output dimension (W2). (Computed from W1, K, STRIDE):
//#define OUTPUT_DIM 16
//#define LOG_OUTPUT_DIM 4
#define OUTPUT_DIM 4
#define LOG_OUTPUT_DIM 2

// Element bit size.
#define ELEMENT_BIT_SIZE 64
#define WEIGHT_ELEMENT_BIT_SIZE 64

// Elements per AXI packet.
#define ELEMENT_PER_PACKET 1

// Additional defines of other functions
#define DIM 4
#define LOG_DIM 2
#define FILTER_BURST_NUM 1


#endif
//////////////// end of test constants //////////////////////

//#define LOG_DIM 10
//#define DIM 1024

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
// Largest dimension.
#define MAX_DIM MAX(MAX(FILTER_DIM, INPUT_DIM), MAX(N_CHAN, OUTPUT_DIM))

// We always use unsigned int as element type.
// The bitsize must be passed via ELEM_BIT_SIZE.
/* typedef ap_uint<ELEMENT_BIT_SIZE> Elem; */
//typedef unsigned long long Elem; // u64
typedef ap_uint<ELEMENT_BIT_SIZE> Elem;
typedef ap_uint<WEIGHT_ELEMENT_BIT_SIZE> Wt_Elem;


// AXI packet interface.
struct AxiPacket {
  Elem data[ELEMENT_PER_PACKET];
};


//////////////////////////////////////////////////////////////////////
// Inlined functions.
//////////////////////////////////////////////////////////////////////

#define MASK 4294967295           // 2^32-1
#define PRIME 2305843009213693951 // 2^61-1

// Efficient modular arithmetic function for p=2^61-1. Only works for this value
// of p. This function might return a number slightly greater than p (possibly
// by an additive factor of 8); It'd be cleaner to check if the answer is
// greater than p and if so subtract p, but I don't want to pay that efficiency
// hit here, so the user should just be aware of this.
inline Elem mod(Elem x) {
//Elem mod(Elem x) {
//#pragma HLS INLINE
//   #pragma HLS PIPELINE
  return (x >> 61) + (x & PRIME); 
}

// Efficient modular multiplication function mod 2^61-1.
inline Elem mod_mlt(Elem x, Elem y) {
//Elem mod_mlt(Elem x, Elem y) {
//#pragma HLS INLINE
//   #pragma HLS PIPELINE
  Elem hi_x = x >> 32;
  Elem hi_y = y >> 32;
  Elem low_x = x & MASK;
  Elem low_y = y & MASK;

  // Since mod might return something slightly large than 2^61-1,
  // we need to multiply by 8 in two pieces to avoid overflow.
  Elem piece1 = mod((hi_x * hi_y) << 3);
  Elem z = (hi_x * low_y + hi_y * low_x);
  Elem hi_z = z >> 32;
  Elem low_z = z & MASK;

  // Note 2^64 mod (2^61-1) is 8.
  Elem piece2 = mod((hi_z << 3) + mod((low_z << 32)));
  Elem piece3 = mod(low_x * low_y);
  Elem result = mod(piece1 + piece2 + piece3);

  return result;
}

// Computes chi_v(r), where chi is the Lagrange polynomial that takes boolean
// vector v to 1 and all other boolean vectors to 0. (we view v's bits as
// defining a boolean vector. n is dimension of this vector. All arithmetic done
// mod p.
inline Elem Phase1(const Elem bit_vector, const Elem r[], int num_of_bits) {
// Elem Phase1(const Elem bit_vector, const Elem r[], int num_of_bits) {
  Elem x = bit_vector;
  Elem c = 1;
  for (int i = 0; i < num_of_bits; i++) {
//   #pragma HLS PIPELINE
    if (x & 1)
      c = mod_mlt(c, r[i]);
    else
      c = mod_mlt(c, 1 + PRIME - r[i]);
    x = x >> 1;
  }
  return c;
}





//////////////////////////////////////////////////////////////////////
// Function prototypes
//////////////////////////////////////////////////////////////////////

void convo_sum_check_prover_hw(
    int round, Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH],
    Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT],
    const Elem
        random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    const Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH],
    Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3]);


void convo_sum_check_prover_hw_wrapped(
    AxiPacket in_stream[(INPUT_DIM * INPUT_DIM * N_CHAN +
                         FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
                        ELEMENT_PER_PACKET],
    AxiPacket in_stream_prover[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN],
    Elem random_q,
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET],
    Elem h_function[3]); 


void convo_sum_check_prover_small_hw(
    int round, int accumulate_filters,
    Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH],
    Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM],
    const Elem
        random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    const Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH],
    Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3]);


void convo_sum_check_prover_small_hw_wrapped(
    AxiPacket in_stream[INPUT_DIM * INPUT_DIM * N_CHAN / ELEMENT_PER_PACKET],
    AxiPacket wt_stream[N_CHAN * FILTER_DIM * FILTER_DIM / ELEMENT_PER_PACKET],
    AxiPacket in_stream_prover[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN],
    Elem random_q,
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET],
    Elem h_function[3]); 


// The prover Biggie + Quad.
void convo_sum_check_prover_quad_hw(
    int proveQuad,
    int round, Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH],
    Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT],
    const Elem
        random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    const Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH],
    Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3],
    int round_quad,
    const Elem random_values_quad[LOG_DIM],
    const Elem random_q_quad[LOG_DIM],
    const Elem V_quad[DIM],
    const Elem F_quad[LOG_DIM][4]);



void convo_sum_check_prover_quad_hw_wrapped(
    AxiPacket in_stream[(INPUT_DIM * INPUT_DIM * N_CHAN +
                         FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
                        ELEMENT_PER_PACKET],
    AxiPacket in_stream_prover[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN],
    Elem random_q,
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET],
    Elem h_function[3]); 

//Elem sum_check_activation_hw(int proveIt, int rounds,  
//                            const Elem q[LOG_DIM], const Elem r[LOG_DIM], 
//                            const Elem V[DIM], const Elem F[LOG_DIM][4]);
//

