#include <stdio.h>
#include <stdlib.h>

#include "convo.h"

// Convolution function to be accelerated in HW.
void convo_mi_hw(Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN],
                 Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                 Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT]) {
#pragma HLS INLINE

//LOOP_N_FILT:
//  for (int nf = 0; nf < N_FILT; nf++) {
Elem sum[N_FILT];

  LOOP_set_sum:
    for (int nf = 0; nf < N_FILT; nf++) {
  sum[nf]=0;}

  LOOP_INPUT_ROW:
    for (int ih = 0; ih < INPUT_DIM - FILTER_DIM + 1; ih++) {
    LOOP_INPUT_COL:
      for (int iw = 0; iw < INPUT_DIM - FILTER_DIM + 1; iw++) {
      LOOP_FILT_ROW:
        for (int fh = 0; fh < FILTER_DIM; fh++) {
        LOOP_FILT_COL:
          for (int fw = 0; fw < FILTER_DIM; fw++) {
        LOOP_N_FILT:
          for (int nf = 0; nf < N_FILT; nf++) {
          LOOP_CHAN:
            for (int nc = 0; nc < N_CHAN; nc++) {
              //sum += ac[ih + fh][iw + fw][nc] * wt[fh][fw][nc][nf];
              sum[nf] += ac[ih + fh][iw + fw][nc] * wt[fh][fw][nc][nf];
            }
          }
        }
      }
        LOOP_N_FILT2:
          for (int nf = 0; nf < N_FILT; nf++) {
        out[ih][iw][nf] = sum[nf];
        sum[nf]=0;}
    }
  }
  return;
}

// Convolution function to be accelerated in HW.
void convo_L1_hw(Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN],
                 Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                 Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT]) {
#pragma HLS INLINE

//LOOP_N_FILT:
//  for (int nf = 0; nf < N_FILT; nf++) {
Elem sum[N_FILT];

  LOOP_set_sum:
    for (int nf = 0; nf < N_FILT; nf++) {
  sum[nf]=0;}

  LOOP_INPUT_ROW:
    for (int ih = 0; ih < INPUT_DIM - FILTER_DIM + 1; ih++) {
    LOOP_INPUT_COL:
      for (int iw = 0; iw < INPUT_DIM - FILTER_DIM + 1; iw++) {
      LOOP_FILT_ROW:
        for (int fh = 0; fh < FILTER_DIM; fh++) {
        LOOP_FILT_COL:
          for (int fw = 0; fw < FILTER_DIM; fw++) {
          LOOP_CHAN:
            for (int nc = 0; nc < N_CHAN; nc++) {
              LOOP_N_FILT:
                for (int nf = 0; nf < N_FILT; nf++) {
                //sum += ac[ih + fh][iw + fw][nc] * wt[fh][fw][nc][nf];
                sum[nf] += ac[ih + fh][iw + fw][nc] * wt[fh][fw][nc][nf];
            }
          }
        }
      }
        LOOP_N_FILT2:
          for (int nf = 0; nf < N_FILT; nf++) {
        out[ih][iw][nf] = sum[nf];
        sum[nf]=0;}
    }
  }
  return;
}

// Convolution function to be accelerated in HW.
void convo_mi_mod_hw(Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN],
                 Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                 Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT]) {
#pragma HLS INLINE

Elem sum[N_FILT];

  LOOP_set_sum:
    for (int nf = 0; nf < N_FILT; nf++) {
  sum[nf]=0;}

  LOOP_INPUT_ROW:
    for (int ih = 0; ih < INPUT_DIM - FILTER_DIM + 1; ih++) {
    LOOP_INPUT_COL:
      for (int iw = 0; iw < INPUT_DIM - FILTER_DIM + 1; iw++) {
      LOOP_FILT_ROW:
        for (int fh = 0; fh < FILTER_DIM; fh++) {
        LOOP_FILT_COL:
          for (int fw = 0; fw < FILTER_DIM; fw++) {
        LOOP_N_FILT:
          for (int nf = 0; nf < N_FILT; nf++) {
          LOOP_CHAN:
            for (int nc = 0; nc < N_CHAN; nc++) {
              sum[nf] = mod( sum[nf] + mod_mlt(ac[ih + fh][iw + fw][nc] , wt[fh][fw][nc][nf]));
            }
          }
        }
      }
        LOOP_N_FILT2:
          for (int nf = 0; nf < N_FILT; nf++) {
        out[ih][iw][nf] = sum[nf];
        sum[nf]=0;}
    }
  }
  return;
}


// Reference implementation of convolution, taken from:
// https://github.com/hls-fpga-machine-learning/hls4ml/blob/master/nnet_utils/nnet_conv.h
void convo_ref_hw(Elem data[INPUT_DIM][INPUT_DIM][N_CHAN],
                  Wt_Elem weights[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT],
                  Elem biases[N_FILT]) {

  // Convert data to 1D.
  Elem data_1d[INPUT_DIM * INPUT_DIM * N_CHAN];
  // #pragma HLS ARRAY_PARTITION variable=data_1d complete dim=0
  for (int ih = 0; ih < INPUT_DIM; ih++) {
    for (int iw = 0; iw < INPUT_DIM; iw++) {
      for (int cc = 0; cc < N_CHAN; cc++) {
        data_1d[ih * INPUT_DIM * N_CHAN + iw * N_CHAN + cc] = data[ih][iw][cc];
      }
    }
  }

  Elem
      mult[OUTPUT_DIM * OUTPUT_DIM * N_FILT * N_CHAN * FILTER_DIM * FILTER_DIM];
  Elem acc[OUTPUT_DIM * OUTPUT_DIM * N_FILT];

  // #pragma HLS ARRAY_PARTITION variable=mult complete dim=0
  // #pragma HLS ARRAY_PARTITION variable=acc  complete dim=0

  // Use a function_instantiate in case it helps to explicitly optimize
  // unchanging weights/biases #pragma HLS function_instantiate
  // variable=weights,biases

  // Parallel mode
  // #pragma HLS PIPELINE
  // #pragma HLS ARRAY_PARTITION variable=biases complete dim=0

  // Limit multipliers to control parallelization.
  const int multiplier_limit = 99999;
  // const int multiplier_limit =
  //     compute_multiplier_limit_conv2d<CONFIG_T>(weights);
// #pragma HLS ALLOCATION instances=mul limit=multiplier_limit operation

// Convolve, saving all multiplication results to accumulate later
ConvOutHeight:
  for (int oh = 0; oh < OUTPUT_DIM; oh++) {
  ConvOutWidth:
    for (int ow = 0; ow < OUTPUT_DIM; ow++) {
    ConvFilt:
      for (int ff = 0; ff < N_FILT; ff++) {
      ConvChan:
        for (int cc = 0; cc < N_CHAN; cc++) {
        ConvFiltHeight:
          for (int fh = 0; fh < FILTER_DIM; fh++) {
          ConvFiltWidth:
            for (int fw = 0; fw < FILTER_DIM; fw++) {

              int index_mult =
                  oh * OUTPUT_DIM * N_FILT * N_CHAN * FILTER_DIM * FILTER_DIM +
                  ow * N_FILT * N_CHAN * FILTER_DIM * FILTER_DIM +
                  ff * N_CHAN * FILTER_DIM * FILTER_DIM +
                  cc * FILTER_DIM * FILTER_DIM + fh * FILTER_DIM + fw;

              if ((oh * STRIDE_HEIGHT + fh) < PAD_TOP ||
                  (oh * STRIDE_HEIGHT + fh) >= (PAD_TOP + INPUT_DIM) ||
                  (ow * STRIDE_WIDTH + fw) < PAD_LEFT ||
                  (ow * STRIDE_WIDTH + fw) >= (PAD_LEFT + INPUT_DIM)) {
                mult[index_mult] = 0;
              } else {
                mult[index_mult] =
                    data_1d[(oh * STRIDE_HEIGHT + fh - PAD_TOP) * INPUT_DIM *
                                N_CHAN +
                            (ow * STRIDE_WIDTH + fw - PAD_LEFT) * N_CHAN + cc] *
                    weights[fh][fw][cc][ff];
              }

            } // end mult loop
          }   // end channel loop
        }     // end filter width loop
      }       // end filter height loop
    }         // end output width loop
  }           // end output height loop

  // Initialize accumulator with input biases
  for (int oh = 0; oh < OUTPUT_DIM; oh++) {
    for (int ow = 0; ow < OUTPUT_DIM; ow++) {
      for (int ff = 0; ff < N_FILT; ff++) {
        acc[oh * OUTPUT_DIM * N_FILT + ow * N_FILT + ff] = biases[ff];
      }
    }
  }

// Accumulate multiplication result
AccumOutHeight:
  for (int oh = 0; oh < OUTPUT_DIM; oh++) {
  AccumOutWidth:
    for (int ow = 0; ow < OUTPUT_DIM; ow++) {
    AccumFilt:
      for (int ff = 0; ff < N_FILT; ff++) {
      // Do "dot product" sum within filter and sum over channels
      AccumChan:
        for (int cc = 0; cc < N_CHAN; cc++) {
        AccumDotHeight:
          for (int fh = 0; fh < FILTER_DIM; fh++) {
          AccumDotWidth:
            for (int fw = 0; fw < FILTER_DIM; fw++) {

              int index_mult =
                  oh * OUTPUT_DIM * N_FILT * N_CHAN * FILTER_DIM * FILTER_DIM +
                  ow * N_FILT * N_CHAN * FILTER_DIM * FILTER_DIM +
                  ff * N_CHAN * FILTER_DIM * FILTER_DIM +
                  cc * FILTER_DIM * FILTER_DIM + fh * FILTER_DIM + fw;

              acc[oh * OUTPUT_DIM * N_FILT + ow * N_FILT + ff] +=
                  mult[index_mult];

            } // end dot product filter width loop
          }   // end dot product filter height loop
        }     // end n channel loop
      }       // end n filter loop
    }         // end output width loop
  }           // end output height loop

  // Cast to "res_t" type
  for (int oh = 0; oh < OUTPUT_DIM; oh++) {
    for (int ow = 0; ow < OUTPUT_DIM; ow++) {
      for (int ff = 0; ff < N_FILT; ff++) {
        out[oh][ow][ff] =
            (Elem)(acc[oh * OUTPUT_DIM * N_FILT + ow * N_FILT + ff]);
      }
    }
  }
}

void convo_mi_hw_wrapped(
    AxiPacket in_stream[(INPUT_DIM * INPUT_DIM * N_CHAN +
                         FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
                        ELEMENT_PER_PACKET],
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET]) {

#pragma HLS INTERFACE axis port = in_stream
#pragma HLS INTERFACE axis port = out_stream
#pragma HLS INTERFACE s_axilite port = return bundle = CONTROL_BUS

#pragma HLS data_pack variable = in_stream struct_level
#pragma HLS data_pack variable = out_stream struct_level

  AxiPacket packet, packet1, packet2, packet3, packet4;

  Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN];
  Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];
  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];

  int is_idx = 0;
LOOP_IN_AC:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j++) {
      LOOP_IN_AC_ST: for (int k = 0; k < N_CHAN; k += ELEMENT_PER_PACKET) {
        packet = in_stream[is_idx++];
      LOOP_IN_AC_ASSIGN:
        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
          ac[i][j][k+m] = packet.data[m];
        }
      }
    }
  }

LOOP_IN_WT_ROW:
  for (int i = 0; i < FILTER_DIM; i++) {
  LOOP_IN_WT_COL:
      for (int j = 0; j < FILTER_DIM; j++) {
      LOOP_IN_WT_N_CHAN:
        for (int k = 0; k < N_CHAN; k++) {
          LOOP_IN_WT_ST:
            for (int m = 0; m < N_FILT; m += ELEMENT_PER_PACKET) {
            packet = in_stream[is_idx++];
      //    LOOP_IN_WT_ST:
      //      for (int m = 0; m < N_FILT; m ++) {
      //LOOP_IN_WT_N_CHAN:
      //  for (int k = 0; k < N_CHAN; k+=ELEMENT_PER_PACKET) {
      //      packet1 = in_stream[is_idx++];
            LOOP_IN_WT_ASSIGN:
              for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
                wt[i][j][k+n][m] = packet.data[n];
                //wt[i][j][k+n+0][m] = packet1.data[n](15,0);
                //wt[i][j][k+n+1][m] = packet2.data[n](31,16);
                //wt[i][j][k+n+2][m] = packet3.data[n](47,32);
                //wt[i][j][k+n+3][m] = packet4.data[n](63,48);
            //wt[i][j][k][m + n] = packet.data[n];
            }
          }
        }
      }
    }

convo_mi_hw(ac,wt,out);

  int os_idx = 0;
LOOP_OUT_OP_ROW:
  for (int i = 0; i < OUTPUT_DIM; i++) {
  LOOP_OUT_OP_COL:
    for (int j = 0; j < OUTPUT_DIM; j++) {
    LOOP_OUT_OP_ST:
      for (int k = 0; k < N_FILT; k += ELEMENT_PER_PACKET) {
      LOOP_OUT_OP_ASSIGN:
        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
          packet.data[m] = out[i][j][k + m];
        }
        out_stream[os_idx] = packet;
        os_idx++;
      }
    }
  }

  return;
}
