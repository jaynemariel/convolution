#include <ap_axi_sdata.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////
// PARAMETERS.
//////////////////////////////////////////////////////////////////////

// Matrix dimension (both x, y).
#ifndef INPUT_DIM
#error "INPUT_DIM must be defined!"
#endif

// How many elements to send at once in one packet.
#ifndef ELEMENT_PER_PACKET
#error "ELEMENT_PER_PACKET must be defined!"
#endif

// Matrix element type.
// typedef float Elem;  // 32 bit floating-point.
// typedef double Elem;  // 64 bit floating-point.
// typedef ap_int<64> Elem; // 64 bit signed integer.

// We always use unsigned int.
// The bitsize must be passed via ELEM_BIT_SIZE.
#ifndef ELEMENT_BIT_SIZE
#error "ELEMENT_BIT_SIZE must be defined!"
#endif
// ELEMENT_BIT_SIZE bit unsigned integer.
typedef ap_uint<ELEMENT_BIT_SIZE> Elem;

// AXI packet interface.
struct AxiPacket {
  Elem data[ELEMENT_PER_PACKET];
};

//////////////////////////////////////////////////////////////////////
// Function prototypes
//////////////////////////////////////////////////////////////////////
void mmult_hw(Elem a[INPUT_DIM][INPUT_DIM], Elem b[INPUT_DIM][INPUT_DIM],
              Elem c[INPUT_DIM][INPUT_DIM]);

void mmult_hw_wrapped(
    AxiPacket in_stream[2 * INPUT_DIM * INPUT_DIM / ELEMENT_PER_PACKET],
    AxiPacket out_stream[INPUT_DIM * INPUT_DIM / ELEMENT_PER_PACKET]);
