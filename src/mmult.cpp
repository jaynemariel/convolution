#include <stdio.h>
#include <stdlib.h>

#include "mmult.h"

// Standalone matrix multiplication HW.
void mmult_hw(Elem a[INPUT_DIM][INPUT_DIM], Elem b[INPUT_DIM][INPUT_DIM],
              Elem out[INPUT_DIM][INPUT_DIM]) {
#pragma HLS INLINE
  int const FACTOR = INPUT_DIM / 2;
  // All pragmas are defined in run_hls.tcl as directives, and can be
  // enable/disabled via the Makefile.
  //#pragma HLS array_partition variable=a block factor=FACTOR dim=2
  //#pragma HLS array_partition variable=b block factor=FACTOR dim=1

  // Matrix multiplication of a A*B matrix.
L1:
  for (int ia = 0; ia < INPUT_DIM; ++ia)
  L2:
    for (int ib = 0; ib < INPUT_DIM; ++ib) {
      //#pragma HLS PIPELINE II=1
      Elem sum = 0;
    L3:
      for (int id = 0; id < INPUT_DIM; ++id) {
        sum += a[ia][id] * b[id][ib];
      }
      out[ia][ib] = sum;
    }

  return;
}

// Matrix multiplier HW wrapped in AXI4-Stream interface.
void mmult_hw_wrapped(
    AxiPacket in_stream[2 * INPUT_DIM * INPUT_DIM / ELEMENT_PER_PACKET],
    AxiPacket out_stream[INPUT_DIM * INPUT_DIM / ELEMENT_PER_PACKET]) {
#pragma HLS INTERFACE axis port = in_stream
#pragma HLS INTERFACE axis port = out_stream
#pragma HLS INTERFACE s_axilite port = return bundle = CONTROL_BUS

#pragma HLS data_pack variable = in_stream struct_level
#pragma HLS data_pack variable = out_stream struct_level

  // Assume ELEMENT_PER_PACKET dimensions, so it's easier to do the unpacking.
  assert(INPUT_DIM % ELEMENT_PER_PACKET == 0);

  AxiPacket packet;
  int is_idx = 0;
  int os_idx = 0;

  Elem a[INPUT_DIM][INPUT_DIM];
  Elem b[INPUT_DIM][INPUT_DIM];
  Elem out[INPUT_DIM][INPUT_DIM];

  // Stream in the first matrix.
STREAM_IN_1:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE II = 1
      packet = in_stream[is_idx++];
      for (int k = 0; k < ELEMENT_PER_PACKET; k++) {
#pragma HLS UNROLL
        a[i][j + k] = packet.data[k];
      }
    }
  }

  // Stream in the second matrix.
STREAM_IN_2:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE II = 1
      packet = in_stream[is_idx++];
      for (int k = 0; k < ELEMENT_PER_PACKET; k++) {
#pragma HLS UNROLL
        b[i][j + k] = packet.data[k];
      }
    }
  }

  // Do the HW multiplication!!
  mmult_hw(a, b, out);

  // Stream out the result matrix.
STREAM_OUT:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE II = 1
      for (int k = 0; k < ELEMENT_PER_PACKET; k++) {
#pragma HLS UNROLL
        packet.data[k] = out[i][j + k];
      }
      out_stream[os_idx++] = packet;
    }
  }

  return;
}
