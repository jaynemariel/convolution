#include "verified_convo.h"

void update_sum(Elem sum[], const Elem q[]) {
//#pragma HLS INLINE
  Elem eval_b[OUTPUT_DIM];
  for (int k = 0; k < OUTPUT_DIM; k++)
    eval_b[k] = Phase1(k, q, LOG_OUTPUT_DIM);

  UPDATE_SUM_1:
  for (int i = 0; i < INPUT_DIM; i++) {
    UPDATE_SUM_1_1:
    for (int j = 0; j < FILTER_DIM; j++) {
//   #pragma HLS PIPELINE
      Elem ans = 0;
      UPDATE_SUM_1_1_1:
      for (int k = 0; k < OUTPUT_DIM; k++) {
//   #pragma HLS PIPELINE
        ans = mod(
            ans +
            mod_mlt(eval_b[k],
                    sum[i * (OUTPUT_DIM * FILTER_DIM) + k * FILTER_DIM + j]));
      }
      sum[i * FILTER_DIM + j] = ans;
    }
  }
}

// The prover Biggie.
void convo_sum_check_prover_hw(
    int round, Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH],
    Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT],
    const Elem
        random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    const Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH],
    Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3]) {

  static Elem sum_1[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];
  static Elem sum_2[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];

  static Elem temp[3][MAX_DIM]; // For temporary storage of h_func
  static Elem partial[FILTER_DIM][FILTER_DIM]; // For temporary storage of h_func
  static Elem temp_0, temp_1, temp_2;



  // Initialize things in the first round.
  if (round == 0) {
  INIT_SUMS_1:
    for (int i = 0; i < FILTER_DIM; i++) {
    INIT_SUMS_1_1:
      for (int j = 0; j < OUTPUT_DIM; j++) {
   #pragma HLS PIPELINE
        int x = (i + j * STRIDE) % INPUT_DIM;
        sum_1[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
        sum_2[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
      }
    }

// Phase 1 for sum 1
    // s(y,j,v) S1jCoeff.
    update_sum(sum_1, random_q);

    // s(x,i,u) S2iCoeff.
    update_sum(sum_2, &random_q[LOG_OUTPUT_DIM]);


    // Initialize filters.
    {
      Elem eval_b[N_FILT]; //kCoeff.
    INIT_FILTERS_1_P1:
      for (int k = 0; k < N_FILT; k++) {
        eval_b[k] = Phase1(k, &random_q[2 * LOG_OUTPUT_DIM], LOG_N_FILT);
      }
    INIT_FILTERS_2_P2_Rows:
      for (int i = 0; i < FILTER_DIM; i++) {
      INIT_FILTERS_Cols:
        for (int j = 0; j < FILTER_DIM; j++) {
        INIT_FILTERS_Chan:
          for (int t = 0; t < N_CHAN; t++) {
            Elem ans = 0;
            int index = t * (FILTER_DIM * FILTER_DIM) + i * FILTER_DIM + j;
          INIT_FILTERS_Kernels:
            for (int k = 0; k < N_FILT; k++) {
              ans = mod(
                  ans +
                  mod_mlt(
                      eval_b[k],
                      filters[k * (FILTER_DIM * FILTER_DIM * N_CHAN) + index]));
            }
            filters[index] = ans;
          }
        }
      }
    }

    // Initialize activation tensor.
    {
// If batch is more than 1 then we take include the effect of random vector B. 
  if (N_BATCH > 1){
      Elem eval_b[N_BATCH]; // bCoeff.
    INIT_ACTIVATION_1_P1:
      for (int k = 0; k < N_BATCH; k++) {
        eval_b[k] =
            Phase1(k, &random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT], LOG_N_BATCH);
      }
    INIT_ACTIVATION_2_P2_Rows:
      for (int i = 0; i < INPUT_DIM; i++) {
      INIT_ACTIVATION_2_1_Cols:
        for (int j = 0; j < INPUT_DIM; j++) {
        INIT_ACTIVATION_2_1_1_Chan:
          for (int t = 0; t < N_CHAN; t++) {
            Elem ans = 0;
            int index = i * (INPUT_DIM * N_CHAN) + j * N_CHAN + t;
          INIT_ACTIVATION_2_1_1_1_Batches:
            for (int k = 0; k < N_BATCH; k++) {
              ans = mod(
                  ans +
                  mod_mlt(
                      eval_b[k],
                      activation_tensor[k * (INPUT_DIM * INPUT_DIM * N_CHAN) +
                                        index]));
            }
            activation_tensor[index] = ans;
          }
        }
      }
    }
  }
  }

  if (0 <= round && round < LOG_FILTER_DIM) {
    static int p1 = FILTER_DIM;
    const int max_loop_count_1 = FILTER_DIM/ 2;
    int at_;
for (int i = 0; i < FILTER_DIM; i++)
  for (int j = 0; j < FILTER_DIM/2; j++)
    partial[i][j] = 0;


  R_var1_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var1_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var1_t:
        for (int t = 0; t < N_CHAN; t++) {
        Elem at_ = activation_tensor[x * INPUT_DIM * N_CHAN + y * N_CHAN + t];
        R_var1_u:
          for (int u = 0; u < FILTER_DIM; u++) {
             temp_0 = mod_mlt( at_, sum_2[x * FILTER_DIM + u]);
          R_var1_v:
            for (int v = 0; v < (FILTER_DIM >> 1); v++) {
if (v<(p1 >> 1)){
            //for (int v = 0; v < (p1 >> 1); v++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_1
              int filter_idx = t * FILTER_DIM * p1 + u * p1 + v * 2;
              int sum_idx = y * p1 + v * 2;
              
              temp[0][v] =
                  mod(temp[0][v] + mod_mlt(temp_0, mod_mlt(filters[filter_idx],
                                                         sum_1[sum_idx])));
              temp[1][v] = mod(temp[1][v] +
                               mod_mlt(temp_0, mod_mlt(filters[filter_idx + 1],
                                                       sum_1[sum_idx + 1])));
              temp[2][v] =
                  mod(temp[2][v] +
                      mod_mlt(temp_0, mod_mlt(mod(2 * filters[filter_idx + 1] +
                                                  PRIME - filters[filter_idx]),
                                              mod(2 * sum_1[sum_idx + 1] +
                                                  PRIME - sum_1[sum_idx]))));
}
            }
          }
        }
      }
    }

  R_var1_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_1
    R_var1_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

  const int max_loop_filter = N_CHAN * FILTER_DIM * max_loop_count_1;
  R_var1_update:
    for (int i = 0; i < N_CHAN * FILTER_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_filter
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }

  const int max_loop_sum = INPUT_DIM * max_loop_count_1;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_sum
      sum_1[i] = mod(mod_mlt(sum_1[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_1[2 * i + 1], random_values[round]));
    }
  } // end of 1. rounds.

  if (LOG_FILTER_DIM <= round && round < 2 * LOG_FILTER_DIM) {
    static int p1 = FILTER_DIM;
    const int max_loop_count_2 = FILTER_DIM / 2;
  R_var2_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var2_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var2_t:
        for (int t = 0; t < N_CHAN; t++) {
        R_var2_u:
          //for (int u = 0; u < (p1 >> 1); u++) {
          for (int u = 0; u < (FILTER_DIM >> 1); u++) {
if (u<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_2
            temp_0 = mod_mlt(
                activation_tensor[x * INPUT_DIM * N_CHAN + y * N_CHAN + t],
                sum_1[y]);

            temp[0][u] = mod(temp[0][u] +
                             mod_mlt(temp_0, mod_mlt(filters[t * p1 + u * 2],
                                                     sum_2[x * p1 + u * 2])));

            temp[1][u] =
                mod(temp[1][u] +
                    mod_mlt(temp_0, mod_mlt(filters[t * p1 + u * 2 + 1],
                                            sum_2[x * p1 + u * 2 + 1])));

            temp[2][u] = mod(
                temp[2][u] +
                mod_mlt(temp_0, mod_mlt(mod(2 * filters[t * p1 + u * 2 + 1] +
                                            PRIME - filters[t * p1 + u * 2]),
                                        mod(2 * sum_2[x * p1 + u * 2 + 1] +
                                            PRIME - sum_2[x * p1 + u * 2]))));
}
          }
        }
      }
    }

  R_var2_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_2
    R_var2_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

    const int max_loop_filter = N_CHAN * max_loop_count_2;
    for (int i = 0; i < N_CHAN * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_filter
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }
    const int max_loop_sum = INPUT_DIM * max_loop_count_2;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_sum
      sum_2[i] = mod(mod_mlt(sum_2[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_2[2 * i + 1], random_values[round]));
    }
  } // end of 2. rounds.

  if (2 * LOG_FILTER_DIM <= round && round < 2 * LOG_FILTER_DIM + LOG_N_CHAN) {
    static int p1 = N_CHAN;
    const int max_loop_count_3 = N_CHAN / 2;
  R_var3_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var3_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var3_t:
        //for (int t = 0; t < (p1 >> 1); t++) {
        for (int t = 0; t < (N_CHAN >> 1); t++) {
if (t<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
          int input_idx = x * INPUT_DIM * p1 + y * p1 + 2 * t;
          
          temp_0 = mod_mlt(sum_2[x], sum_1[y]);

          temp[0][t] =
              mod(temp[0][t] +
                  mod_mlt(temp_0, mod_mlt(filters[t * 2],
                                          activation_tensor[input_idx])));

          temp[1][t] =
              mod(temp[1][t] +
                  mod_mlt(temp_0, mod_mlt(filters[t * 2 + 1],
                                          activation_tensor[input_idx + 1])));

          temp[2][t] =
              mod(temp[2][t] +
                  mod_mlt(temp_0,
                          mod_mlt(mod(2 * filters[t * 2 + 1] + PRIME -
                                      filters[t * 2]),
                                  mod(2 * activation_tensor[input_idx + 1] +
                                      PRIME - activation_tensor[input_idx]))));
}
        }
      }
    }

  R_var3_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
    R_var3_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

  R_var3_update_filt:
    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }

    const int max_loop_activation = INPUT_DIM * INPUT_DIM * max_loop_count_3;
    Elem one_minus_r = (1 + PRIME -random_values[round]);
    Elem rand_val   = random_values[round];
  R_var3_update_at:
    for (int i = 0; i < INPUT_DIM * INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_activation
#pragma HLS PIPELINE
      activation_tensor[i] = mod(
          //mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          //mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
          mod_mlt(activation_tensor[2 * i], one_minus_r) +
          mod_mlt(activation_tensor[2 * i + 1], rand_val));
    }
  } // end of 3. rounds.

  if (2 * LOG_FILTER_DIM + LOG_N_CHAN <= round &&
      round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM) {
    static int p1 = INPUT_DIM;
    const int max_loop_count_4 = INPUT_DIM / 2;
  R_var4_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var4_y:
      //for (int y = 0; y < (p1 >> 1); y++) {
      for (int y = 0; y < (INPUT_DIM >> 1); y++) {
if (y<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
        int input_idx = x * p1 + y * 2;

        temp[0][y] = mod(temp[0][y] +
                         mod_mlt(sum_2[x], mod_mlt(activation_tensor[input_idx],
                                                   sum_1[y * 2])));
        temp[1][y] =
            mod(temp[1][y] +
                mod_mlt(sum_2[x], mod_mlt(activation_tensor[input_idx + 1],
                                          sum_1[y * 2 + 1])));
        temp[2][y] = mod(
            temp[2][y] +
            mod_mlt(sum_2[x],
                    mod_mlt(mod(2 * activation_tensor[input_idx + 1] + PRIME -
                                activation_tensor[input_idx]),
                            mod(2 * sum_1[y * 2 + 1] + PRIME - sum_1[y * 2]))));
}
      }
    }

  R_var4_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
    R_var4_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    for (int j = 0; j < 3; j++)
      h_function[round][j] = mod_mlt(h_function[round][j], filters[0]);

    p1 = p1 >> 1;

    const int max_loop_activation = INPUT_DIM * max_loop_count_4;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_activation
      activation_tensor[i] = mod(
          mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
    }

    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
      sum_1[i] = mod(mod_mlt(sum_1[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_1[2 * i + 1], random_values[round]));
    }
  } // end of 4. rounds.

  if (2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM <= round &&
      round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM) {
    static int p1 = INPUT_DIM;
    const int max_loop_count_5 = INPUT_DIM / 2;
  R_var5_x:
    //for (int x = 0; x < (p1 >> 1); x++) {
    for (int x = 0; x < (INPUT_DIM >> 1); x++) {
if (x<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      temp[0][x] =
          mod(temp[0][x] + mod_mlt(activation_tensor[x * 2], sum_2[x * 2]));
      temp[1][x] = mod(temp[1][x] +
                       mod_mlt(activation_tensor[x * 2 + 1], sum_2[x * 2 + 1]));
      temp[2][x] =
          mod(temp[2][x] +
              mod_mlt(mod(2 * activation_tensor[x * 2 + 1] + PRIME -
                          activation_tensor[x * 2]),
                      mod(2 * sum_2[x * 2 + 1] + PRIME - sum_2[x * 2])));
}
    }

  R_var5_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
    R_var5_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    Elem tmp = mod_mlt(filters[0], sum_1[0]);
  R_var5_3: // h_func final
    for (int j = 0; j < 3; j++)
      h_function[round][j] = mod_mlt(h_function[round][j], tmp);

    p1 = p1 >> 1;

    Elem one_minus_r = (1 + PRIME -random_values[round]);
    Elem rand_val   = random_values[round];
    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      activation_tensor[i] = mod(
          //mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          //mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
          mod_mlt(activation_tensor[2 * i], one_minus_r) +
          mod_mlt(activation_tensor[2 * i + 1], rand_val));
    }

    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      sum_2[i] = mod(mod_mlt(sum_2[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_2[2 * i + 1], random_values[round]));
    }
  } // end of 5. rounds.

//} //FIXME is this needed????


}


// The Axi4 Wrapper Biggie.
void convo_sum_check_prover_hw_wrapped(
    AxiPacket in_stream[(INPUT_DIM * INPUT_DIM * N_CHAN +
                         FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
                        ELEMENT_PER_PACKET],
    AxiPacket in_stream_prover[4*LOG_FILTER_DIM + 4*LOG_INPUT_DIM + 2*LOG_N_CHAN],
    //AxiPacket in_stream_prover[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN],
    //Elem random_q[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET],
    Elem h_function[3]) {

#pragma HLS INTERFACE axis port = in_stream
#pragma HLS INTERFACE axis port = out_stream
#pragma HLS INTERFACE s_axilite port = return bundle = CONTROL_BUS

#pragma HLS data_pack variable = in_stream struct_level
#pragma HLS data_pack variable = out_stream struct_level

  AxiPacket packet;

  //Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN];
  //Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];
  //Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];
  Elem ac[INPUT_DIM*INPUT_DIM*N_CHAN];
  Elem wt[FILTER_DIM*FILTER_DIM*N_CHAN*N_FILT];
  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];

  Elem random_values[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN];
  Elem random_q[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN];
  Elem h_func[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3];
  int round;

  int is_idx = 0;
LOOP_IN_AC:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j++) {
      LOOP_IN_AC_ST: for (int k = 0; k < N_CHAN; k += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        packet = in_stream[is_idx++];
      LOOP_IN_AC_ASSIGN:
        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
          //ac[i][j][k+m] = packet.data[m];
          ac[i*INPUT_DIM*N_CHAN + j*N_CHAN + k + m] = packet.data[m];
        }
      }
    }
  }

LOOP_IN_WT:
  for (int i = 0; i < FILTER_DIM; i++) {
    for (int j = 0; j < FILTER_DIM; j++) {
      for (int k = 0; k < N_CHAN; k++) {
        LOOP_IN_WT_ST: for (int m = 0; m < N_FILT; m += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
          packet = in_stream[is_idx++];
        LOOP_IN_WT_ASSIGN:
          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
            //wt[i][j][k+n][m] = packet.data[n];
            wt[i*FILTER_DIM*N_CHAN*N_FILT + j*N_CHAN*N_FILT + k*ELEMENT_PER_PACKET+m+n] = packet.data[n];
          }
        }
      }
    }
  }

LOOP_IN_PROTOCOL:
  for (int i = 0; i < 2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN; 
                                                i += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        packet = in_stream_prover[i];
        LOOP_IN_PROTOCOL_ASSIGN:
          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
            random_values[i+n] = packet.data[n];
          }
  }

int idxp = 2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN;
LOOP_IN_PROTOCOL2:
  for (int i = 0; i < 2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN; 
                                                i += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        packet = in_stream_prover[idxp + i];
        LOOP_IN_PROTOCOL_ASSIGN2:
          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
            random_q[i+n] = packet.data[n];
          }
  }

  for (int round = 0;
       round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM; round++) {
       convo_sum_check_prover_hw(round,ac,wt,random_values,random_q,h_func);
    
  for (int i = 0 ; i < 3 ; i++){
    h_function[i] = h_func[round][i];
  }
}

  int os_idx = 0;
LOOP_OUT_OP:
  for (int i = 0; i < OUTPUT_DIM; i++) {
    for (int j = 0; j < OUTPUT_DIM; j++) {
      LOOP_OUT_OP_ST: for (int k = 0; k < N_FILT; k += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        LOOP_OUT_OP_ASSIGN:
          for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
            packet.data[m] = out[i][j][k + m]; // h_function
          }
        out_stream[os_idx] = packet;
        os_idx++;
      }
    }
  }

  return;
}







// The prover Smalls.
void convo_sum_check_prover_small_hw(
    int round, int accumulate_filters,
    Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH],
    Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM],
    const Elem
        random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    const Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH],
    Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3]) {

  static Elem sum_1[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];
  static Elem sum_2[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];

  static Elem temp[3][MAX_DIM]; // For temporary storage of h_func
  static Elem partial[FILTER_DIM][FILTER_DIM]; // For temporary storage of h_func
  static Elem temp_0, temp_1, temp_2;




  // Initialize things in the first round.
  if (round == 0) {
  INIT_SUMS_1:
    for (int i = 0; i < FILTER_DIM; i++) {
    INIT_SUMS_1_1:
      for (int j = 0; j < OUTPUT_DIM; j++) {
   #pragma HLS PIPELINE
        int x = (i + j * STRIDE) % INPUT_DIM;
        sum_1[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
        sum_2[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
      }
    }

// Phase 1 for sum 1
    // s(y,j,v) S1jCoeff.
    update_sum(sum_1, random_q);

    // s(x,i,u) S2iCoeff.
    update_sum(sum_2, &random_q[LOG_OUTPUT_DIM]);


    // Initialize filters.
    {
      Elem eval_b[N_FILT]; //kCoeff.
    INIT_FILTERS_1_P1:
      for (int k = 0; k < N_FILT; k++) {
        eval_b[k] = Phase1(k, &random_q[2 * LOG_OUTPUT_DIM], LOG_N_FILT);
      }
    // Run Phase 2 for filters for FILTER_BURST_NUM times, by checking counter
    if (accumulate_filters < FILTER_BURST_NUM ){
    INIT_FILTERS_2_P2_Rows:
      for (int i = 0; i < FILTER_DIM; i++) {
      INIT_FILTERS_Cols:
        for (int j = 0; j < FILTER_DIM; j++) {
        INIT_FILTERS_Chan:
          for (int t = 0; t < N_CHAN; t++) {
            Elem ans = 0;
            int index = t * (FILTER_DIM * FILTER_DIM) + i * FILTER_DIM + j;
          INIT_FILTERS_Kernels:
            for (int k = 0; k < N_FILT; k++) {
              ans = mod(
                  ans +
                  mod_mlt(
                      eval_b[k],
                      filters[k * (FILTER_DIM * FILTER_DIM * N_CHAN) + index]));
            }
            filters[index] = ans;
          }
        }
      }
}
    }

    // Initialize activation tensor.
    {
// If batch is more than 1 then we take include the effect of random vector B. 
  if (N_BATCH > 1){
      Elem eval_b[N_BATCH]; // bCoeff.
    INIT_ACTIVATION_1_P1:
      for (int k = 0; k < N_BATCH; k++) {
        eval_b[k] =
            Phase1(k, &random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT], LOG_N_BATCH);
      }
    INIT_ACTIVATION_2_P2_Rows:
      for (int i = 0; i < INPUT_DIM; i++) {
      INIT_ACTIVATION_2_1_Cols:
        for (int j = 0; j < INPUT_DIM; j++) {
        INIT_ACTIVATION_2_1_1_Chan:
          for (int t = 0; t < N_CHAN; t++) {
            Elem ans = 0;
            int index = i * (INPUT_DIM * N_CHAN) + j * N_CHAN + t;
          INIT_ACTIVATION_2_1_1_1_Batches:
            for (int k = 0; k < N_BATCH; k++) {
              ans = mod(
                  ans +
                  mod_mlt(
                      eval_b[k],
                      activation_tensor[k * (INPUT_DIM * INPUT_DIM * N_CHAN) +
                                        index]));
            }
            activation_tensor[index] = ans;
          }
        }
      }
    }
  }
  }

  if (0 <= round && round < LOG_FILTER_DIM && (accumulate_filters == FILTER_BURST_NUM)) {
    static int p1 = FILTER_DIM;
    const int max_loop_count_1 = FILTER_DIM/ 2;
    int at_;
for (int i = 0; i < FILTER_DIM; i++)
  for (int j = 0; j < FILTER_DIM/2; j++)
    partial[i][j] = 0;


  R_var1_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var1_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var1_t:
        for (int t = 0; t < N_CHAN; t++) {
        Elem at_ = activation_tensor[x * INPUT_DIM * N_CHAN + y * N_CHAN + t];
        R_var1_u:
          for (int u = 0; u < FILTER_DIM; u++) {
             temp_0 = mod_mlt( at_, sum_2[x * FILTER_DIM + u]);
          R_var1_v:
            for (int v = 0; v < (FILTER_DIM >> 1); v++) {
if (v<(p1 >> 1)){
            //for (int v = 0; v < (p1 >> 1); v++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_1
              int filter_idx = t * FILTER_DIM * p1 + u * p1 + v * 2;
              int sum_idx = y * p1 + v * 2;
              
              temp[0][v] =
                  mod(temp[0][v] + mod_mlt(temp_0, mod_mlt(filters[filter_idx],
                                                         sum_1[sum_idx])));
              temp[1][v] = mod(temp[1][v] +
                               mod_mlt(temp_0, mod_mlt(filters[filter_idx + 1],
                                                       sum_1[sum_idx + 1])));
              temp[2][v] =
                  mod(temp[2][v] +
                      mod_mlt(temp_0, mod_mlt(mod(2 * filters[filter_idx + 1] +
                                                  PRIME - filters[filter_idx]),
                                              mod(2 * sum_1[sum_idx + 1] +
                                                  PRIME - sum_1[sum_idx]))));
}
            }
          }
        }
      }
    }

  R_var1_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_1
    R_var1_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

  const int max_loop_filter = N_CHAN * FILTER_DIM * max_loop_count_1;
  R_var1_update:
    for (int i = 0; i < N_CHAN * FILTER_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_filter
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }

  const int max_loop_sum = INPUT_DIM * max_loop_count_1;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_sum
      sum_1[i] = mod(mod_mlt(sum_1[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_1[2 * i + 1], random_values[round]));
    }
  } // end of 1. rounds.

  if (LOG_FILTER_DIM <= round && round < 2 * LOG_FILTER_DIM) {
    static int p1 = FILTER_DIM;
    const int max_loop_count_2 = FILTER_DIM / 2;
  R_var2_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var2_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var2_t:
        for (int t = 0; t < N_CHAN; t++) {
        R_var2_u:
          //for (int u = 0; u < (p1 >> 1); u++) {
          for (int u = 0; u < (FILTER_DIM >> 1); u++) {
if (u<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_2
            temp_0 = mod_mlt(
                activation_tensor[x * INPUT_DIM * N_CHAN + y * N_CHAN + t],
                sum_1[y]);

            temp[0][u] = mod(temp[0][u] +
                             mod_mlt(temp_0, mod_mlt(filters[t * p1 + u * 2],
                                                     sum_2[x * p1 + u * 2])));

            temp[1][u] =
                mod(temp[1][u] +
                    mod_mlt(temp_0, mod_mlt(filters[t * p1 + u * 2 + 1],
                                            sum_2[x * p1 + u * 2 + 1])));

            temp[2][u] = mod(
                temp[2][u] +
                mod_mlt(temp_0, mod_mlt(mod(2 * filters[t * p1 + u * 2 + 1] +
                                            PRIME - filters[t * p1 + u * 2]),
                                        mod(2 * sum_2[x * p1 + u * 2 + 1] +
                                            PRIME - sum_2[x * p1 + u * 2]))));
}
          }
        }
      }
    }

  R_var2_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_2
    R_var2_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

    const int max_loop_filter = N_CHAN * max_loop_count_2;
    for (int i = 0; i < N_CHAN * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_filter
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }
    const int max_loop_sum = INPUT_DIM * max_loop_count_2;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_sum
      sum_2[i] = mod(mod_mlt(sum_2[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_2[2 * i + 1], random_values[round]));
    }
  } // end of 2. rounds.

  if (2 * LOG_FILTER_DIM <= round && round < 2 * LOG_FILTER_DIM + LOG_N_CHAN) {
    static int p1 = N_CHAN;
    const int max_loop_count_3 = N_CHAN / 2;
  R_var3_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var3_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var3_t:
        //for (int t = 0; t < (p1 >> 1); t++) {
        for (int t = 0; t < (N_CHAN >> 1); t++) {
if (t<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
          int input_idx = x * INPUT_DIM * p1 + y * p1 + 2 * t;
          
          temp_0 = mod_mlt(sum_2[x], sum_1[y]);

          temp[0][t] =
              mod(temp[0][t] +
                  mod_mlt(temp_0, mod_mlt(filters[t * 2],
                                          activation_tensor[input_idx])));

          temp[1][t] =
              mod(temp[1][t] +
                  mod_mlt(temp_0, mod_mlt(filters[t * 2 + 1],
                                          activation_tensor[input_idx + 1])));

          temp[2][t] =
              mod(temp[2][t] +
                  mod_mlt(temp_0,
                          mod_mlt(mod(2 * filters[t * 2 + 1] + PRIME -
                                      filters[t * 2]),
                                  mod(2 * activation_tensor[input_idx + 1] +
                                      PRIME - activation_tensor[input_idx]))));
}
        }
      }
    }

  R_var3_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
    R_var3_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

  R_var3_update_filt:
    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }

    const int max_loop_activation = INPUT_DIM * INPUT_DIM * max_loop_count_3;
    Elem one_minus_r = (1 + PRIME -random_values[round]);
    Elem rand_val   = random_values[round];
  R_var3_update_at:
    for (int i = 0; i < INPUT_DIM * INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_activation
#pragma HLS PIPELINE
      activation_tensor[i] = mod(
          //mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          //mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
          mod_mlt(activation_tensor[2 * i], one_minus_r) +
          mod_mlt(activation_tensor[2 * i + 1], rand_val));
    }
  } // end of 3. rounds.

  if (2 * LOG_FILTER_DIM + LOG_N_CHAN <= round &&
      round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM) {
    static int p1 = INPUT_DIM;
    const int max_loop_count_4 = INPUT_DIM / 2;
  R_var4_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var4_y:
      //for (int y = 0; y < (p1 >> 1); y++) {
      for (int y = 0; y < (INPUT_DIM >> 1); y++) {
if (y<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
        int input_idx = x * p1 + y * 2;

        temp[0][y] = mod(temp[0][y] +
                         mod_mlt(sum_2[x], mod_mlt(activation_tensor[input_idx],
                                                   sum_1[y * 2])));
        temp[1][y] =
            mod(temp[1][y] +
                mod_mlt(sum_2[x], mod_mlt(activation_tensor[input_idx + 1],
                                          sum_1[y * 2 + 1])));
        temp[2][y] = mod(
            temp[2][y] +
            mod_mlt(sum_2[x],
                    mod_mlt(mod(2 * activation_tensor[input_idx + 1] + PRIME -
                                activation_tensor[input_idx]),
                            mod(2 * sum_1[y * 2 + 1] + PRIME - sum_1[y * 2]))));
}
      }
    }

  R_var4_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
    R_var4_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    for (int j = 0; j < 3; j++)
      h_function[round][j] = mod_mlt(h_function[round][j], filters[0]);

    p1 = p1 >> 1;

    const int max_loop_activation = INPUT_DIM * max_loop_count_4;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_activation
      activation_tensor[i] = mod(
          mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
    }

    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
      sum_1[i] = mod(mod_mlt(sum_1[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_1[2 * i + 1], random_values[round]));
    }
  } // end of 4. rounds.

  if (2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM <= round &&
      round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM) {
    static int p1 = INPUT_DIM;
    const int max_loop_count_5 = INPUT_DIM / 2;
  R_var5_x:
    //for (int x = 0; x < (p1 >> 1); x++) {
    for (int x = 0; x < (INPUT_DIM >> 1); x++) {
if (x<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      temp[0][x] =
          mod(temp[0][x] + mod_mlt(activation_tensor[x * 2], sum_2[x * 2]));
      temp[1][x] = mod(temp[1][x] +
                       mod_mlt(activation_tensor[x * 2 + 1], sum_2[x * 2 + 1]));
      temp[2][x] =
          mod(temp[2][x] +
              mod_mlt(mod(2 * activation_tensor[x * 2 + 1] + PRIME -
                          activation_tensor[x * 2]),
                      mod(2 * sum_2[x * 2 + 1] + PRIME - sum_2[x * 2])));
}
    }

  R_var5_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
    R_var5_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    Elem tmp = mod_mlt(filters[0], sum_1[0]);
  R_var5_3: // h_func final
    for (int j = 0; j < 3; j++)
      h_function[round][j] = mod_mlt(h_function[round][j], tmp);

    p1 = p1 >> 1;

    Elem one_minus_r = (1 + PRIME -random_values[round]);
    Elem rand_val   = random_values[round];
    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      activation_tensor[i] = mod(
          //mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          //mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
          mod_mlt(activation_tensor[2 * i], one_minus_r) +
          mod_mlt(activation_tensor[2 * i + 1], rand_val));
    }

    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      sum_2[i] = mod(mod_mlt(sum_2[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_2[2 * i + 1], random_values[round]));
    }
  } // end of 5. rounds.
}


// The Axi4 Wrapper Smalls.
void convo_sum_check_prover_small_hw_wrapped(
    AxiPacket in_stream[INPUT_DIM * INPUT_DIM * N_CHAN / ELEMENT_PER_PACKET],
    AxiPacket wt_stream[FILTER_DIM * FILTER_DIM * N_CHAN],
    AxiPacket in_stream_prover[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN],
    Elem random_q[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET],
    Elem h_function[3]) {

#pragma HLS INTERFACE axis port = in_stream
#pragma HLS INTERFACE axis port = out_stream
#pragma HLS INTERFACE s_axilite port = return bundle = CONTROL_BUS

#pragma HLS data_pack variable = in_stream struct_level
#pragma HLS data_pack variable = out_stream struct_level

  AxiPacket packet;

  //Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN];
  //Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];
  //Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];
  Elem ac[INPUT_DIM*INPUT_DIM*N_CHAN];
  Elem wt[FILTER_DIM*FILTER_DIM*N_CHAN*N_FILT];
  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];

  Elem random_values[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN];
  Elem h_func[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3];
  int round;

  int in_idx = 0;
LOOP_IN_AC:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j++) {
      LOOP_IN_AC_ST: for (int k = 0; k < N_CHAN; k += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        packet = in_stream[in_idx++];
      LOOP_IN_AC_ASSIGN:
        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
          //ac[i][j][k+m] = packet.data[m];
          ac[i*INPUT_DIM*N_CHAN + j*N_CHAN + k + m] = packet.data[m];
        }
      }
    }
  }

  int wt_idx = 0;
LOOP_IN_WT:
  for (int i = 0; i < FILTER_DIM; i++) {
    for (int j = 0; j < FILTER_DIM; j++) {
      for (int k = 0; k < N_CHAN; k++) {
        LOOP_IN_WT_ST: for (int m = 0; m < N_FILT; m += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
          packet = wt_stream[wt_idx++];
        LOOP_IN_WT_ASSIGN:
          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
            //wt[i][j][k+n][m] = packet.data[n];
            wt[i*FILTER_DIM*N_CHAN*N_FILT + j*N_CHAN*N_FILT + k*ELEMENT_PER_PACKET+m+n] = packet.data[n];
          }
        }
      }
    }
  }

LOOP_IN_PROTOCOL:
  for (int i = 0; i < 2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN; 
                                                i += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        packet = in_stream_prover[i];
        LOOP_IN_PROTOCOL_ASSIGN:
          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
            random_values[i+n] = packet.data[n];
          }
  }

{
LOOP_FILT_BURST:
  for (int k = 0 ; k < FILTER_BURST_NUM ; k++){
#pragma HLS INLINE
     convo_sum_check_prover_small_hw(-1,k,
                                ac,wt,random_values,random_q,h_func);
  }
}



LOOP_PROVER_ROUNDS:
  for (int round = 0;
       round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM; round++) {
       convo_sum_check_prover_small_hw(round,FILTER_BURST_NUM,
                                    ac,wt,random_values,random_q,h_func);
    
  for (int i = 0 ; i < 3 ; i++){
    h_function[i] = h_func[round][i];
  }
}

  int os_idx = 0;
LOOP_OUT_OP_ROW:
  for (int i = 0; i < OUTPUT_DIM; i++) {
  LOOP_OUT_OP_COL:
    for (int j = 0; j < OUTPUT_DIM; j++) {
    LOOP_OUT_OP_ST:
      for (int k = 0; k < N_FILT; k += ELEMENT_PER_PACKET) {
#pragma HLS PIPELINE
        LOOP_OUT_OP_ASSIGN:
          for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
            packet.data[m] = out[i][j][k + m]; // h_function
          }
        out_stream[os_idx] = packet;
        os_idx++;
      }
    }
  }

  return;
}

// Protocol reduces verifying a claim that v_i-1(q)=a_i-1 to verifying that
// v_i(q')=a_i
void sum_check_activation_hw(int proveIt,int rounds, Elem q_quad[LOG_DIM], Elem r_quad[LOG_DIM], Elem V_quad[DIM],
                          Elem F_quad[LOG_DIM][4]) {

#pragma HLS INLINE
  // I values
  static Elem I_quad[DIM];
  for (int i = 0; i < DIM; i++){
   #pragma HLS PIPELINE
    I_quad[i] = 1;
}


  // initialize I values optimized method
  Elem I_tmp;
  Elem steps = 1;
  int max_loop_count_i = DIM / 2;
LOOP_QUAD_INIT_I:
  for (int i = 0; i < LOG_DIM; i++) {
    Elem temp_q0 = mod(1 + PRIME -q_quad[i]);
    Elem temp_q1 = q_quad[i];
LOOP_QUAD_INIT_I_1:
    for (int k = 0; k < steps; k++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_i
      I_tmp = I_quad[k];

      //I_quad[k] = mod_mlt(I_tmp, 1 + PRIME - q_quad[i]);
      //I_quad[k + steps] = mod_mlt(I_tmp, q_quad[i]);
      I_quad[k] = mod_mlt(I_tmp, temp_q0);
      I_quad[k + steps] = mod_mlt(I_tmp, temp_q1);
    }
    steps = steps << 1;
  }

  // partial sums for calculating F at each round
  Elem parsumV[4];
  Elem parsumI[4];

  steps = DIM;
  int j;

  int max_loop_count_r = DIM / 2;
LOOP_QUAD_ROUNDS:
  for (int i = 0; i < LOG_DIM; i++) { // rounds of the protocol
    steps = steps >> 1;
LOOP_QUAD_ROUNDS_ACCUM:
//Elem tempr0 = mod(1 + PRIME - r_quad[i]);
//Elem tempr1 = r_quad[i];
    for (int k = 0; k < steps; k++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_r
      j = 2 * k;
      parsumV[0] = V_quad[j];
      parsumV[1] = V_quad[j + 1];
      parsumV[2] = mod(2 * V_quad[j + 1] + PRIME - V_quad[j]);
      parsumV[3] = mod(3 * V_quad[j + 1] + 2 * (PRIME - V_quad[j]));

      parsumI[0] = I_quad[j];
      parsumI[1] = I_quad[j + 1];
      parsumI[2] = mod(2 * I_quad[j + 1] + PRIME - I_quad[j]);
      parsumI[3] = mod(3 * I_quad[j + 1] + 2 * (PRIME - I_quad[j]));

      // check using update_V
      V_quad[k] = mod(mod_mlt(V_quad[j], 1 + PRIME - r_quad[i]) + mod_mlt(V_quad[j + 1], r_quad[i]));
      I_quad[k] = mod(mod_mlt(I_quad[j], 1 + PRIME - r_quad[i]) + mod_mlt(I_quad[j + 1], r_quad[i]));

      //V_quad[k] = mod(mod_mlt(V_quad[j], tempr0) + mod_mlt(V_quad[j + 1],tempr1 ));
      //I_quad[k] = mod(mod_mlt(I_quad[j], tempr0) + mod_mlt(I_quad[j + 1],tempr1 ));
LOOP_QUAD_ROUNDS_WRITE_F:
      for (int m = 0; m < 4; m++){
        F_quad[i][m] =
            mod(F_quad[i][m] + mod_mlt(mod_mlt(parsumV[m], parsumV[m]), parsumI[m]));
        }
    }
  }
}


// The prover Biggie + Quad.
void convo_sum_check_prover_quad_hw(
    int proveIt,
    int round, Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH],
    Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT],
    const Elem
        random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
    const Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH],
    Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3],
    int rounds_quad,
    Elem q[LOG_DIM],
    Elem r[LOG_DIM],
    Elem V[DIM],
    Elem F[LOG_DIM][4]
) {

  static Elem sum_1[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];
  static Elem sum_2[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];

  static Elem temp[3][MAX_DIM]; // For temporary storage of h_func
  static Elem partial[FILTER_DIM][FILTER_DIM]; // For temporary storage of h_func
  static Elem temp_0, temp_1, temp_2;


if (proveIt == 0){

  // Initialize things in the first round.
  if (round == 0) {
  INIT_SUMS_1:
    for (int i = 0; i < FILTER_DIM; i++) {
    INIT_SUMS_1_1:
      for (int j = 0; j < OUTPUT_DIM; j++) {
   #pragma HLS PIPELINE
        int x = (i + j * STRIDE) % INPUT_DIM;
        sum_1[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
        sum_2[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
      }
    }

// Phase 1 for sum 1
    // s(y,j,v) S1jCoeff.
    update_sum(sum_1, random_q);

    // s(x,i,u) S2iCoeff.
    update_sum(sum_2, &random_q[LOG_OUTPUT_DIM]);


    // Initialize filters.
    {
      Elem eval_b[N_FILT]; //kCoeff.
    INIT_FILTERS_1_P1:
      for (int k = 0; k < N_FILT; k++) {
        eval_b[k] = Phase1(k, &random_q[2 * LOG_OUTPUT_DIM], LOG_N_FILT);
      }
    INIT_FILTERS_2_P2_Rows:
      for (int i = 0; i < FILTER_DIM; i++) {
      INIT_FILTERS_Cols:
        for (int j = 0; j < FILTER_DIM; j++) {
        INIT_FILTERS_Chan:
          for (int t = 0; t < N_CHAN; t++) {
            Elem ans = 0;
            int index = t * (FILTER_DIM * FILTER_DIM) + i * FILTER_DIM + j;
          INIT_FILTERS_Kernels:
            for (int k = 0; k < N_FILT; k++) {
              ans = mod(
                  ans +
                  mod_mlt(
                      eval_b[k],
                      filters[k * (FILTER_DIM * FILTER_DIM * N_CHAN) + index]));
            }
            filters[index] = ans;
          }
        }
      }
    }

    // Initialize activation tensor.
    {
// If batch is more than 1 then we take include the effect of random vector B. 
  if (N_BATCH > 1){
      Elem eval_b[N_BATCH]; // bCoeff.
    INIT_ACTIVATION_1_P1:
      for (int k = 0; k < N_BATCH; k++) {
        eval_b[k] =
            Phase1(k, &random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT], LOG_N_BATCH);
      }
    INIT_ACTIVATION_2_P2_Rows:
      for (int i = 0; i < INPUT_DIM; i++) {
      INIT_ACTIVATION_2_1_Cols:
        for (int j = 0; j < INPUT_DIM; j++) {
        INIT_ACTIVATION_2_1_1_Chan:
          for (int t = 0; t < N_CHAN; t++) {
            Elem ans = 0;
            int index = i * (INPUT_DIM * N_CHAN) + j * N_CHAN + t;
          INIT_ACTIVATION_2_1_1_1_Batches:
            for (int k = 0; k < N_BATCH; k++) {
              ans = mod(
                  ans +
                  mod_mlt(
                      eval_b[k],
                      activation_tensor[k * (INPUT_DIM * INPUT_DIM * N_CHAN) +
                                        index]));
            }
            activation_tensor[index] = ans;
          }
        }
      }
    }
  }
  }

  if (0 <= round && round < LOG_FILTER_DIM) {
    static int p1 = FILTER_DIM;
    const int max_loop_count_1 = FILTER_DIM/ 2;
    int at_;
for (int i = 0; i < FILTER_DIM; i++)
  for (int j = 0; j < FILTER_DIM/2; j++)
    partial[i][j] = 0;


  R_var1_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var1_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var1_t:
        for (int t = 0; t < N_CHAN; t++) {
        Elem at_ = activation_tensor[x * INPUT_DIM * N_CHAN + y * N_CHAN + t];
        R_var1_u:
          for (int u = 0; u < FILTER_DIM; u++) {
             temp_0 = mod_mlt( at_, sum_2[x * FILTER_DIM + u]);
          R_var1_v:
            for (int v = 0; v < (FILTER_DIM >> 1); v++) {
if (v<(p1 >> 1)){
            //for (int v = 0; v < (p1 >> 1); v++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_1
              int filter_idx = t * FILTER_DIM * p1 + u * p1 + v * 2;
              int sum_idx = y * p1 + v * 2;
              
              temp[0][v] =
                  mod(temp[0][v] + mod_mlt(temp_0, mod_mlt(filters[filter_idx],
                                                         sum_1[sum_idx])));
              temp[1][v] = mod(temp[1][v] +
                               mod_mlt(temp_0, mod_mlt(filters[filter_idx + 1],
                                                       sum_1[sum_idx + 1])));
              temp[2][v] =
                  mod(temp[2][v] +
                      mod_mlt(temp_0, mod_mlt(mod(2 * filters[filter_idx + 1] +
                                                  PRIME - filters[filter_idx]),
                                              mod(2 * sum_1[sum_idx + 1] +
                                                  PRIME - sum_1[sum_idx]))));
}
            }
          }
        }
      }
    }

  R_var1_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_1
    R_var1_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

  const int max_loop_filter = N_CHAN * FILTER_DIM * max_loop_count_1;
  R_var1_update:
    for (int i = 0; i < N_CHAN * FILTER_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_filter
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }

  const int max_loop_sum = INPUT_DIM * max_loop_count_1;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_sum
      sum_1[i] = mod(mod_mlt(sum_1[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_1[2 * i + 1], random_values[round]));
    }
  } // end of 1. rounds.

  if (LOG_FILTER_DIM <= round && round < 2 * LOG_FILTER_DIM) {
    static int p1 = FILTER_DIM;
    const int max_loop_count_2 = FILTER_DIM / 2;
  R_var2_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var2_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var2_t:
        for (int t = 0; t < N_CHAN; t++) {
        R_var2_u:
          //for (int u = 0; u < (p1 >> 1); u++) {
          for (int u = 0; u < (FILTER_DIM >> 1); u++) {
if (u<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_2
            temp_0 = mod_mlt(
                activation_tensor[x * INPUT_DIM * N_CHAN + y * N_CHAN + t],
                sum_1[y]);

            temp[0][u] = mod(temp[0][u] +
                             mod_mlt(temp_0, mod_mlt(filters[t * p1 + u * 2],
                                                     sum_2[x * p1 + u * 2])));

            temp[1][u] =
                mod(temp[1][u] +
                    mod_mlt(temp_0, mod_mlt(filters[t * p1 + u * 2 + 1],
                                            sum_2[x * p1 + u * 2 + 1])));

            temp[2][u] = mod(
                temp[2][u] +
                mod_mlt(temp_0, mod_mlt(mod(2 * filters[t * p1 + u * 2 + 1] +
                                            PRIME - filters[t * p1 + u * 2]),
                                        mod(2 * sum_2[x * p1 + u * 2 + 1] +
                                            PRIME - sum_2[x * p1 + u * 2]))));
}
          }
        }
      }
    }

  R_var2_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_2
    R_var2_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

    const int max_loop_filter = N_CHAN * max_loop_count_2;
    for (int i = 0; i < N_CHAN * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_filter
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }
    const int max_loop_sum = INPUT_DIM * max_loop_count_2;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_sum
      sum_2[i] = mod(mod_mlt(sum_2[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_2[2 * i + 1], random_values[round]));
    }
  } // end of 2. rounds.

  if (2 * LOG_FILTER_DIM <= round && round < 2 * LOG_FILTER_DIM + LOG_N_CHAN) {
    static int p1 = N_CHAN;
    const int max_loop_count_3 = N_CHAN / 2;
  R_var3_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var3_y:
      for (int y = 0; y < INPUT_DIM; y++) {
      R_var3_t:
        //for (int t = 0; t < (p1 >> 1); t++) {
        for (int t = 0; t < (N_CHAN >> 1); t++) {
if (t<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
          int input_idx = x * INPUT_DIM * p1 + y * p1 + 2 * t;
          
          temp_0 = mod_mlt(sum_2[x], sum_1[y]);

          temp[0][t] =
              mod(temp[0][t] +
                  mod_mlt(temp_0, mod_mlt(filters[t * 2],
                                          activation_tensor[input_idx])));

          temp[1][t] =
              mod(temp[1][t] +
                  mod_mlt(temp_0, mod_mlt(filters[t * 2 + 1],
                                          activation_tensor[input_idx + 1])));

          temp[2][t] =
              mod(temp[2][t] +
                  mod_mlt(temp_0,
                          mod_mlt(mod(2 * filters[t * 2 + 1] + PRIME -
                                      filters[t * 2]),
                                  mod(2 * activation_tensor[input_idx + 1] +
                                      PRIME - activation_tensor[input_idx]))));
}
        }
      }
    }

  R_var3_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
    R_var3_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    p1 = p1 >> 1;

  R_var3_update_filt:
    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_3
      filters[i] =
          mod(mod_mlt(filters[2 * i], 1 + PRIME - random_values[round]) +
              mod_mlt(filters[2 * i + 1], random_values[round]));
    }

    const int max_loop_activation = INPUT_DIM * INPUT_DIM * max_loop_count_3;
    Elem one_minus_r = (1 + PRIME -random_values[round]);
    Elem rand_val   = random_values[round];
  R_var3_update_at:
    for (int i = 0; i < INPUT_DIM * INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_activation
#pragma HLS PIPELINE
      activation_tensor[i] = mod(
          //mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          //mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
          mod_mlt(activation_tensor[2 * i], one_minus_r) +
          mod_mlt(activation_tensor[2 * i + 1], rand_val));
    }
  } // end of 3. rounds.

  if (2 * LOG_FILTER_DIM + LOG_N_CHAN <= round &&
      round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM) {
    static int p1 = INPUT_DIM;
    const int max_loop_count_4 = INPUT_DIM / 2;
  R_var4_x:
    for (int x = 0; x < INPUT_DIM; x++) {
    R_var4_y:
      //for (int y = 0; y < (p1 >> 1); y++) {
      for (int y = 0; y < (INPUT_DIM >> 1); y++) {
if (y<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
        int input_idx = x * p1 + y * 2;

        temp[0][y] = mod(temp[0][y] +
                         mod_mlt(sum_2[x], mod_mlt(activation_tensor[input_idx],
                                                   sum_1[y * 2])));
        temp[1][y] =
            mod(temp[1][y] +
                mod_mlt(sum_2[x], mod_mlt(activation_tensor[input_idx + 1],
                                          sum_1[y * 2 + 1])));
        temp[2][y] = mod(
            temp[2][y] +
            mod_mlt(sum_2[x],
                    mod_mlt(mod(2 * activation_tensor[input_idx + 1] + PRIME -
                                activation_tensor[input_idx]),
                            mod(2 * sum_1[y * 2 + 1] + PRIME - sum_1[y * 2]))));
}
      }
    }

  R_var4_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
    R_var4_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    for (int j = 0; j < 3; j++)
      h_function[round][j] = mod_mlt(h_function[round][j], filters[0]);

    p1 = p1 >> 1;

    const int max_loop_activation = INPUT_DIM * max_loop_count_4;
    for (int i = 0; i < INPUT_DIM * p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_activation
      activation_tensor[i] = mod(
          mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
    }

    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_4
      sum_1[i] = mod(mod_mlt(sum_1[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_1[2 * i + 1], random_values[round]));
    }
  } // end of 4. rounds.

  if (2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM <= round &&
      round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM) {
    static int p1 = INPUT_DIM;
    const int max_loop_count_5 = INPUT_DIM / 2;
  R_var5_x:
    //for (int x = 0; x < (p1 >> 1); x++) {
    for (int x = 0; x < (INPUT_DIM >> 1); x++) {
if (x<(p1 >> 1)){
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      temp[0][x] =
          mod(temp[0][x] + mod_mlt(activation_tensor[x * 2], sum_2[x * 2]));
      temp[1][x] = mod(temp[1][x] +
                       mod_mlt(activation_tensor[x * 2 + 1], sum_2[x * 2 + 1]));
      temp[2][x] =
          mod(temp[2][x] +
              mod_mlt(mod(2 * activation_tensor[x * 2 + 1] + PRIME -
                          activation_tensor[x * 2]),
                      mod(2 * sum_2[x * 2 + 1] + PRIME - sum_2[x * 2])));
}
    }

  R_var5_writeback:
    for (int i = 0; i < (p1 >> 1); i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
    R_var5_h_func:
      for (int j = 0; j < 3; j++) {
        h_function[round][j] = mod(h_function[round][j] + temp[j][i]);
        temp[j][i] = 0;
      }
    }

    Elem tmp = mod_mlt(filters[0], sum_1[0]);
  R_var5_3: // h_func final
    for (int j = 0; j < 3; j++)
      h_function[round][j] = mod_mlt(h_function[round][j], tmp);

    p1 = p1 >> 1;

    Elem one_minus_r = (1 + PRIME -random_values[round]);
    Elem rand_val   = random_values[round];
    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      activation_tensor[i] = mod(
          //mod_mlt(activation_tensor[2 * i], 1 + PRIME - random_values[round]) +
          //mod_mlt(activation_tensor[2 * i + 1], random_values[round]));
          mod_mlt(activation_tensor[2 * i], one_minus_r) +
          mod_mlt(activation_tensor[2 * i + 1], rand_val));
    }

    for (int i = 0; i < p1; i++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_5
      sum_2[i] = mod(mod_mlt(sum_2[2 * i], 1 + PRIME - random_values[round]) +
                     mod_mlt(sum_2[2 * i + 1], random_values[round]));
    }
  } // end of 5. rounds.

} // end of prove convo.

// begin prove quad. sum_check_activation
else{

 sum_check_activation_hw(proveIt, rounds_quad, q, r, V, F );

}
}


//// The Axi4 Wrapper Biggie. Quad
//void convo_sum_check_prover_quad_hw_wrapped(
//    AxiPacket in_stream[(INPUT_DIM * INPUT_DIM * N_CHAN +
//                         FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
//                        ELEMENT_PER_PACKET],
//    AxiPacket in_stream_prover[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN],
//    Elem random_q[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN],
//    AxiPacket
//        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET],
//    Elem h_function[3]) {
//
//#pragma HLS INTERFACE axis port = in_stream
//#pragma HLS INTERFACE axis port = out_stream
//#pragma HLS INTERFACE s_axilite port = return bundle = CONTROL_BUS
//
//#pragma HLS data_pack variable = in_stream struct_level
//#pragma HLS data_pack variable = out_stream struct_level
//
//  AxiPacket packet;
//
//  //Elem ac[INPUT_DIM][INPUT_DIM][N_CHAN];
//  //Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];
//  //Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];
//  Elem ac[INPUT_DIM*INPUT_DIM*N_CHAN];
//  Elem wt[FILTER_DIM*FILTER_DIM*N_CHAN*N_FILT];
//  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];
//
//  Elem random_values[2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN];
//  Elem h_func[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN][3];
//  Elem F[2*LOG_INPUT_DIM + LOG_N_CHAN][4];
//  int round;
//
//  int is_idx = 0;
//LOOP_IN_AC:
//  for (int i = 0; i < INPUT_DIM; i++) {
//    for (int j = 0; j < INPUT_DIM; j++) {
//      LOOP_IN_AC_ST: for (int k = 0; k < N_CHAN; k += ELEMENT_PER_PACKET) {
//#pragma HLS PIPELINE
//        packet = in_stream[is_idx++];
//      LOOP_IN_AC_ASSIGN:
//        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
//          //ac[i][j][k+m] = packet.data[m];
//          ac[i*INPUT_DIM*N_CHAN + j*N_CHAN + k + m] = packet.data[m];
//        }
//      }
//    }
//  }
//
//LOOP_IN_WT:
//  for (int i = 0; i < FILTER_DIM; i++) {
//    for (int j = 0; j < FILTER_DIM; j++) {
//      for (int k = 0; k < N_CHAN; k++) {
//        LOOP_IN_WT_ST: for (int m = 0; m < N_FILT; m += ELEMENT_PER_PACKET) {
//#pragma HLS PIPELINE
//          packet = in_stream[is_idx++];
//        LOOP_IN_WT_ASSIGN:
//          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
//            //wt[i][j][k+n][m] = packet.data[n];
//            wt[i*FILTER_DIM*N_CHAN*N_FILT + j*N_CHAN*N_FILT + k*ELEMENT_PER_PACKET+m+n] = packet.data[n];
//          }
//        }
//      }
//    }
//  }
//
//LOOP_IN_PROTOCOL:
//  for (int i = 0; i < 2*LOG_FILTER_DIM + 2*LOG_INPUT_DIM + LOG_N_CHAN; 
//                                                i += ELEMENT_PER_PACKET) {
//#pragma HLS PIPELINE
//        packet = in_stream_prover[i];
//        LOOP_IN_PROTOCOL_ASSIGN:
//          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
//            random_values[i+n] = packet.data[n];
//          }
//  }
//
//int proveQ=0;
//
//if (proveQ==0){
//  for (int round = 0;
//       round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM; round++) {
//       //convo_sum_check_prover_quad_hw(round,ac,wt,random_values,random_q,h_func);
//       convo_sum_check_prover_quad_hw(proveQ,round, ac, wt,random_values,
//    random_q, h_function, rounds_quad, random_q, random_values, ac, F)     
//  for (int i = 0 ; i < 3 ; i++){
//    h_function[i] = h_func[round][i];
//  }
//}
//proveQ=1;
//}
//else {
//
//  for (int rounds_quad = 0;
//       round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM; round++) {
//
//       convo_sum_check_prover_quad_hw(proveQ,round, ac, wt,random_values,
//    random_q, h_function, rounds_quad, random_q, random_values, ac, F)     
//
//  for (int i = 0 ; i < 4 ; i++){
//    F_h[i] = F[round][i];
//  }
//}
//}
//
//  int os_idx = 0;
//LOOP_OUT_OP:
//  for (int i = 0; i < OUTPUT_DIM; i++) {
//    for (int j = 0; j < OUTPUT_DIM; j++) {
//      LOOP_OUT_OP_ST: for (int k = 0; k < N_FILT; k += ELEMENT_PER_PACKET) {
//#pragma HLS PIPELINE
//        LOOP_OUT_OP_ASSIGN:
//          for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
//            packet.data[m] = out[i][j][k + m]; // h_function
//          }
//        out_stream[os_idx] = packet;
//        os_idx++;
//      }
//    }
//  }
//
//  return;
//}







