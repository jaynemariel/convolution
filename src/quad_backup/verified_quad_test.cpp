#include <cstdlib>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#include "verified_quad.h"



// Performs Extended Euclidean Algorithm
// Used for computing multiplicative inverses mod p
void extEuclideanAlg(Elem u, Elem *u1, Elem *u2, Elem *u3) {
  *u1 = 1;
  *u2 = 0;
  *u3 = u;
  Elem v1 = 0;
  Elem v2 = 1;
  Elem v3 = PRIME;
  Elem q;
  Elem t1;
  Elem t2;
  Elem t3;
  do {
    q = *u3 / v3;
    // t1 = *u1 + p - q * v1;
    // t2 = *u2 + p - q*v2;
    // t3 = *u3 + p - q*v3;
    t1 = mod((*u1) + PRIME - mod_mlt(q, v1));
    t2 = mod((*u2) + PRIME - mod_mlt(q, v2));
    t3 = mod((*u3) + PRIME - mod_mlt(q, v3));
    (*u1) = v1;
    (*u2) = v2;
    (*u3) = v3;
    v1 = t1;
    v2 = t2;
    v3 = t3;
  } while (v3 != 0 && v3 != PRIME);
}


// Computes the modular multiplicative inverse of a modulo m,
// using the extended Euclidean algorithm
// only works for p=2^61-1
Elem inv(Elem a) {
  Elem u1;
  Elem u2;
  Elem u3;
  extEuclideanAlg(a, &u1, &u2, &u3);
  if (u3 == 1)
    return mod(u1);
  else
    return 0;
}

Elem extrap(Elem vec[], Elem n, Elem r) {
  Elem result = 0;
  Elem mult = 1;
  for (Elem i = 0; i < n; i++) {
    mult = 1;
    for (Elem j = 0; j < n; j++) {
      if (i > j)
        mult = mod_mlt(mod_mlt(mult, mod(r - j + PRIME)), inv(i - j));
      if (i < j)
        mult =
            mod_mlt(mod_mlt(mult, mod(r - j + PRIME)), inv(mod(i + PRIME - j)));
    }
    result = mod(result + mod_mlt(mult, vec[i]));
  }
  return result;
}

void check_rounds(Elem a1, Elem a2, Elem F[][4], Elem r[], Elem d, Elem g) {
  if (a1 != mod(F[0][0] + F[0][1]))
    std::cout << "first check failed" << std::endl, exit(1);
  Elem check;
  for (int i = 1; i < d; i++) {
    check = extrap(F[i - 1], g, r[i - 1]);
    if ((mod(F[i][0] + F[i][1]) != check) &&
        (mod(F[i][0] + F[i][1]) + PRIME != check))
      std::cout << "check " << i << " failed" << std::endl, exit(1);
  }

  check = extrap(F[d - 1], g, r[d - 1]);
  if (a2 != check)
    std::cout << "last check failed" << std::endl, exit(1);
}

// evaluate the MLE of I at q and random location r in O(logn)
Elem evaluate_I(Elem* q, Elem* r, int d)
{
    Elem ans=1;
    for(Elem k = 0; k < d; k++)
        ans = mod_mlt(ans, mod(mod_mlt(q[k],r[k]) + mod_mlt(1+PRIME-q[k], 1+PRIME-r[k])) );
    return ans; 
}

// evaluates V_i polynomial at location r.
// O(n)
Elem evaluate_V_i(int mi, int ni, Elem level_i[], Elem r[]) {
  Elem *Phase1_eval = (Elem *)calloc(ni, sizeof(Elem));
  int num_terms = 1;
  Phase1_eval[0] = 1;
  Elem temp;

  for (int i = 0; i < mi; i++) {
    for (int j = 0; j < num_terms; j++) {
      temp = Phase1_eval[j];
      Phase1_eval[j] = mod_mlt(temp, 1 + PRIME - r[i]);
      Phase1_eval[j + num_terms] = mod_mlt(temp, r[i]);
    }
    num_terms = num_terms << 1;
  }
  Elem ans = 0;
  for (int k = 0; k < ni; k++)
    ans = mod(ans + mod_mlt(level_i[k], Phase1_eval[k]));
  free(Phase1_eval);
  return ans;
}



// Protocol reduces verifying a claim that v_i-1(q)=a_i-1 to verifying that v_i(q')=a_i
Elem sum_check_activation(Elem* q, Elem* r, int d, Elem n, Elem* I, Elem* V, Elem** F, Elem& Veval)
{
    //initialize I values optimized method
    Elem I_tmp;
    Elem steps=1;
    for (int i=0; i<d; i++)
    {
        for (int k=0; k<steps; k++)
        {
            I_tmp = I[k];

            I[k] = mod_mlt(I_tmp, 1+PRIME-q[i]);
            I[k+steps] = mod_mlt(I_tmp, q[i]);
           
        }
        
        steps = steps << 1;
    }

    
    // partial sums for calculating F at each round
    Elem* parsumV=(Elem*) calloc(4, sizeof(Elem));
    Elem* parsumI=(Elem*) calloc(4, sizeof(Elem));

    steps=n;
    int j;
   
    for (int i=0; i<d; i++)
    {
        steps = steps >> 1;
        for (int k=0; k<steps; k++)
        {
            j = 2*k;
            parsumV[0] = V[j];
            parsumV[1] = V[j+1];
            parsumV[2] = mod(2*V[j+1] + PRIME - V[j]);
            parsumV[3] = mod(3*V[j+1] + 2*(PRIME - V[j]));

            parsumI[0] = I[j];
            parsumI[1] = I[j+1];
            parsumI[2] = mod(2*I[j+1] + PRIME - I[j]);
            parsumI[3] = mod(3*I[j+1] + 2*(PRIME - I[j]));

            /*FIXME*/
            // check using update_V
            V[k] = mod(mod_mlt(V[j], 1+PRIME-r[i]) + mod_mlt(V[j+1], r[i]));
            I[k] = mod(mod_mlt(I[j], 1+PRIME-r[i]) + mod_mlt(I[j+1], r[i]));
            
            for (int m=0; m<4; m++)
                F[i][m] = mod(F[i][m]+mod_mlt(mod_mlt(parsumV[m],parsumV[m]),parsumI[m]));

        }
           
        //calculate Fi(ri) 
        //check[i] = extrap(F[i], 4, r[i]); //moved to verifier
    }
    Veval = V[0];

    free(parsumV);
    free(parsumI);

}

int main()
{

        
    int n = OUTPUT_DIM * OUTPUT_DIM;
    int d = 2 * LOG_OUTPUT_DIM;

    // input values
    Elem V[n];
    Elem Vcopy[n];
    for (int i=0; i<n; i++)
    {
        V[i] = rand() % 100;
        Vcopy[i] = V[i];
    }

    //I values
    Elem I[n];
    for (int i=0; i<n; i++)
        I[i]=1;
    
    // output of quadratic activation    
    Elem A[n];

 
    // sum-check protocol random values for each iteration
    // should be picked from the field [0, F_p], but this picks from [0, RAND_MAX] 
    Elem r[d];
    for (int i=0; i<d; i++)
        r[i] = rand();

    // random point q to evaluate the output matrix C 
    Elem q[d];
    for (int i=0; i<d; i++)
        q[i] = rand();
    
   // polynomial extrapolations for each round
    Elem F[d][4];
    //for (int i=0; i<d; i++)
    //    F[i] = (Elem*) calloc(4, sizeof(Elem));
    
    clock_t t=clock();
    // local computation using field arithmetic
    for (int i=0; i<n; i++)
        A[i] = mod_mlt(V[i],V[i]);
    t = clock()-t;
    //std::cout << "unverifiable time = " << (double)t/CLOCKS_PER_SEC << std::endl;
    std::cout << (double)t/CLOCKS_PER_SEC << " "; 

    // local computation without using field arithmetic
    for (int i=0; i<n; i++)
        A[i] = (V[i]*V[i]);
    t = clock()-t;
    std::cout << (double)t/CLOCKS_PER_SEC << " "; 
    //std::cout << "unverifiable time wo field arith = " << (double)t/CLOCKS_PER_SEC << std::endl;
    
    // evalute a random point in the MLE of the returned matrix
    // for the output layer this is done by the verifier, for intermediate layers during sum-check by prover
    t=clock();
    Elem a1 = evaluate_V_i(d, n, A, q);
    t=clock()-t;
    //std::cout << "V1 (out) time = " << (double)t/CLOCKS_PER_SEC << std::endl;
    std::cout << (double)t/CLOCKS_PER_SEC << " ";

    // For intermediate layers, this claim is returned by the prover. For input, the verifier computes this
    Elem Veval;

    // prover's work for the sum-check protocol
    t=clock();
for (int round = 0 ; round < 2*LOG_OUTPUT_DIM; round ++){
    sum_check_activation_hw(1,round, q, r, V, F);
}
    t = clock()-t;
    //std::cout << "additional P time = " << (double)t/CLOCKS_PER_SEC << std::endl;
    std::cout << (double)t/CLOCKS_PER_SEC << " ";

    // verifier checks the assertion on the MLE of the input
    // evaluate V at r
    t=clock();
    Veval = evaluate_V_i(d, n, Vcopy, r);
    t=clock()-t;
    //std::cout << "V2 (in) time = " << (double)t/CLOCKS_PER_SEC << std::endl;
    std::cout << (double)t/CLOCKS_PER_SEC << " ";
    //std::cout << d << std::endl;
    
    // verifier checks each iteration of the sum-check protocol
    t=clock();
    // evaluate I at (q,r) 
    Elem Ieval = evaluate_I(q,r,d);
    Elem a2 = mod_mlt(mod_mlt(Veval, Veval), Ieval);

    check_rounds(a1, a2, F, r, d, 4);
    
    t = clock()-t;
    //std::cout << "verifier time = " << (double)t/CLOCKS_PER_SEC << std::endl;
    std::cout << (double)t/CLOCKS_PER_SEC << std::endl;

    //print_values(a1, a2, F, r, d, 4);


    free(V);
    free(Vcopy);
    free(I);
    for (int i=0; i<d; i++)
        free(F[i]);
    free(F);
    free(A);
    free(r);
    free(q);
        
    return 0;
}


