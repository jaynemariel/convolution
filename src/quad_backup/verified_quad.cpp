
//// Protocol reduces verifying a claim that v_i-1(q)=a_i-1 to verifying that
///v_i(q')=a_i
// uint64 sum_check_activation(uint64* q, uint64* r, int d, uint64 n, uint64* I,
// uint64* V, uint64** F, uint64& Veval)
void sum_check_activation_hw(
    //    int proveQuad,
    //    int round_quad,
    const Elem random_values_quad[2 * LOG_OUTPUT_DIM],
    const Elem random_q_quad[2 * LOG_OUTPUT_DIM],
    Elem V_quad[OUTPUT_DIM * OUTPUT_DIM], Elem F_quad[2 * LOG_OUTPUT_DIM][4]);
{
  // initialize I values optimized method
  Elem I_tmp;
  Elem steps = 1;
  Elem I[OUTPUT_DIM * OUTPUT_DIM];      // Identitiy Matrix
  Elem V_quad[OUTPUT_DIM * OUTPUT_DIM]; // Activation Tensor to be evaluated
  Elem F_quad[LOG_OUTPUT_DIM][4];       // F function for quadratic activation

  // I values
Loop_Init_I:
  for (int i = 0; i < OUTPUT_DIM * OUTPUT_DIM; i++) {
    I[i] = 1;
  }

  const int max_loop_count_6 = OUTPUT_DIM * OUTPUT_DIM / 2;
Loop_Phase1_I:
  for (int i = 0; i < 2 * LOG_OUTPUT_DIM; i++) {
    for (int k = 0; k < steps; k++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_6
      I_tmp = I[k];

      I[k] = mod_mlt(I_tmp, 1 + PRIME - random_q_quad[i]);
      I[k + steps] = mod_mlt(I_tmp, random_q_quad[i]);
    }

    steps = steps << 1;
  }

  // partial sums for calculating F at each round
  Elem parsumV[4];
  Elem parsumI[4];

  steps = OUTPUT_DIM * OUTPUT_DIM;
  int j;

Loop_R_quad:
  for (int i = 0; i < 2 * LOG_OUTPUT_DIM; i++) {
    steps = steps >> 1; // update size of vector to loop through.
  Loop_R_quad_accum:
    for (int k = 0; k < steps; k++) {
#pragma HLS LOOP_TRIPCOUNT min = 1 max = max_loop_count_6
      j = 2 * k;
      parsumV[0] = V_quad[j];
      parsumV[1] = V_quad[j + 1];
      parsumV[2] = mod(2 * V_quad[j + 1] + PRIME - V_quad[j]);
      parsumV[3] = mod(3 * V_quad[j + 1] + 2 * (PRIME - V_quad[j]));

      parsumI[0] = I[j];
      parsumI[1] = I[j + 1];
      parsumI[2] = mod(2 * I[j + 1] + PRIME - I[j]);
      parsumI[3] = mod(3 * I[j + 1] + 2 * (PRIME - I[j]));

    Update_V_I : {
      V_quad[k] = mod(mod_mlt(V_quad[j], 1 + PRIME - random_values_quad[i]) +
                      mod_mlt(V_quad[j + 1], random_values_quad[i]));

      I[k] = mod(mod_mlt(I[j], 1 + PRIME - random_values_quad[i]) +
                 mod_mlt(I[j + 1], random_values_quad[i]));
    }

    Loop_R_write_F:
      for (int m = 0; m < 4; m++)
        F_quad[i][m] =
            mod(F_quad[i][m] +
                mod_mlt(mod_mlt(parsumV[m], parsumV[m]), parsumI[m]));
    }

    // calculate Fi(ri)
    // check[i] = extrap(F[i], 4, r[i]); //moved to verifier
  }
  // Veval = V[0];

  // free(parsumV);
  // free(parsumI);

} // end of prove quad.
