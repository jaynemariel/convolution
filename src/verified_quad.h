#include <ap_axi_sdata.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////
// PARAMETERS.
//////////////////////////////////////////////////////////////////////

///////////////////// Test constants ///////////////////////////
#ifdef QUICK_TEST

// Output dimension.
#define OUTPUT_DIM 4
#define LOG_OUTPUT_DIM 2

// Number of filters.
#define N_FILT 2
#define LOG_N_FILT 1

#endif
//////////////// end of test constants //////////////////////

//#define DIM 4096
//#define LOG_DIM 12



// This crap is slow:
/* typedef ap_uint<64> Elem; */
typedef unsigned long long int Elem;

//////////////////////////////////////////////////////////////////////
// Inlined functions.
//////////////////////////////////////////////////////////////////////

#define MASK 4294967295           // 2^32-1
#define PRIME 2305843009213693951 // 2^61-1

// Efficient modular arithmetic function for p=2^61-1. Only works for this value
// of p. This function might return a number slightly greater than p (possibly
// by an additive factor of 8); It'd be cleaner to check if the answer is
// greater than p and if so subtract p, but I don't want to pay that efficiency
// hit here, so the user should just be aware of this.
inline Elem mod(Elem x) {
  // Elem mod(Elem x) {
  //#pragma HLS INLINE
  //   #pragma HLS PIPELINE
  return (x >> 61) + (x & PRIME);
}

// Efficient modular multiplication function mod 2^61-1.
inline Elem mod_mlt(Elem x, Elem y) {
  // Elem mod_mlt(Elem x, Elem y) {
  //#pragma HLS INLINE
  //   #pragma HLS PIPELINE
  Elem hi_x = x >> 32;
  Elem hi_y = y >> 32;
  Elem low_x = x & MASK;
  Elem low_y = y & MASK;

  // Since mod might return something slightly large than 2^61-1,
  // we need to multiply by 8 in two pieces to avoid overflow.
  Elem piece1 = mod((hi_x * hi_y) << 3);
  Elem z = (hi_x * low_y + hi_y * low_x);
  Elem hi_z = z >> 32;
  Elem low_z = z & MASK;

  // Note 2^64 mod (2^61-1) is 8.
  Elem piece2 = mod((hi_z << 3) + mod((low_z << 32)));
  Elem piece3 = mod(low_x * low_y);
  Elem result = mod(piece1 + piece2 + piece3);

  return result;
}

//////////////////////////////////////////////////////////////////////
// Function prototypes
//////////////////////////////////////////////////////////////////////

Elem sum_check_activation_hw(int proveIt, int rounds,  Elem q[], Elem r[], Elem V[],
                          Elem F[][4]);
