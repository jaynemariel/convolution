#include <stdio.h>
#include <stdlib.h>

#include "convo.h"

//TODO: Check why the make test-convo command tests both wrapped and unwrapped
// instead of the one determined by the config.py

void convo_ref_sw(Elem at[INPUT_DIM][INPUT_DIM][N_CHAN],
                  Wt_Elem wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT]) {

  // matrix multiplication of a A*B matrix
LOOP_MM_Q:
  for (int q = 0; q < N_FILT; q++) {
  LOOP_MM_1:
    for (int i = 0; i < INPUT_DIM - FILTER_DIM + 1; i++) {
    LOOP_MM_2:
      for (int j = 0; j < INPUT_DIM - FILTER_DIM + 1; j++) {
        int sum = 0;
      LOOP_MM_3:
        for (int k = 0; k < FILTER_DIM; k++) {
        LOOP_MM_4:
          for (int m = 0; m < FILTER_DIM; m++) {
          LOOP_MM_5:
            for (int n = 0; n < N_CHAN; n++) {
              sum += at[i + k][j + m][n] * wt[k][m][n][q];
           // printf(" at %d %d %d %d, sum= %ld at= %ld * wt=%ld\n", 
           //         i+k,j+m,n,q,(unsigned long)sum,
           //         (unsigned long)at[i+k][j+m][n] ,
           //         (unsigned long)wt[k][m][n][q] );
            }
          }
        }
        out[i][j][q] = sum;
      }
    }
  }

  for (int ih = 0; ih < OUTPUT_DIM; ih++) 
    for (int iw = 0; iw < OUTPUT_DIM; iw++) 
      for (int nc = 0; nc < N_FILT; nc++)
         //printf("out : %ld \n",(unsigned long)out[ih][iw][nc]);
  return;
}

#if WRAPPED == 0

int main(void) {
  printf("=== STANDALONE mode ===\n");

  Elem input[INPUT_DIM][INPUT_DIM][N_CHAN];
  Wt_Elem weights[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];
  Elem output_hw[OUTPUT_DIM][OUTPUT_DIM][N_FILT];
  Elem output_sw[OUTPUT_DIM][OUTPUT_DIM][N_FILT];

  // Initialize input.
  for (int ih = 0; ih < INPUT_DIM; ih++) {
    for (int iw = 0; iw < INPUT_DIM; iw++) {
      for (int nc = 0; nc < N_CHAN; nc++) {
        input[ih][iw][nc] = ih + iw;
      }
    }
  }

  // Initialize weights.
  for (int fh = 0; fh < FILTER_DIM; fh++) {
    for (int fw = 0; fw < FILTER_DIM; fw++) {
      for (int nc = 0; nc < N_CHAN; nc++) {
        for (int nf = 0; nf < N_FILT; nf++) {
          weights[fh][fw][nc][nf] = fh + fw + nc + 1;
        }
      }
    }
  }

  // Run HW implementation.
  convo_mi_hw(input, weights, output_hw);

  // -------------------------------------------------------
  // Run the reference implementation!

  // Zero biases.
  Elem biases[N_FILT];
  for (int nf = 0; nf < N_FILT; nf++) {
    biases[nf] = 0;
  }

  //convo_ref_hw(input, weights, output_sw, biases);
  convo_ref_sw(input, weights, output_sw);

  // -------------------------------------------------------

  // Compare results.
  int err = 0;
  for (int oh = 0; (oh < OUTPUT_DIM && !err); oh++)
    for (int ow = 0; (ow < OUTPUT_DIM && !err); ow++)
      for (int nf = 0; (nf < N_FILT && !err); nf++)
        if (output_sw[oh][ow][nf] != output_hw[oh][ow][nf]){
            printf("FAILED! at %d %d %d, sw= %ld hw- %ld\n", 
                    oh,ow,nf,( unsigned long)output_sw[oh][ow][nf] ,(unsigned long)output_hw[oh][ow][nf] );
          err++;}

  if (err == 0) {
    printf("==> TEST PASSED: the reference convolution function output "
           "the same matrix.\n");
  } else {
    printf("FAILED!!\n");
  }

  return err;
}

#else

int main(void) {
  printf("=== WRAPPED mode ===\n");
  int err;

  /* Input Matrices: Activation and Weight*/
  Elem matOp1[INPUT_DIM][INPUT_DIM][N_CHAN];
  Wt_Elem matOp2[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];

  /* Output Matrices: Reference Software and Implemented hardwarte*/
  Elem out_sw[OUTPUT_DIM][OUTPUT_DIM][N_FILT];
  Elem out_hw[OUTPUT_DIM][OUTPUT_DIM][N_FILT];

  /** Matrix Initiation */

  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j++) {
      for (int c = 0; c < N_CHAN; c++) {
        matOp1[i][j][c] = (float)(i + j);
      }
    }
  }

  for (int i = 0; i < FILTER_DIM; i++) {
    for (int j = 0; j < FILTER_DIM; j++) {
      for (int c = 0; c < N_CHAN; c++) {
        for (int q = 0; q < N_FILT; q++) {
          matOp2[i][j][c][q] = (float)((i + j + c + 1));
        }
      }
    }
  }
  /** End of Initiation */

  printf("DEBUGGING AXI4 STREAMING DATA TYPES!\r\n");

  // prepare data for the DUT
  AxiPacket inp_stream[(INPUT_DIM * INPUT_DIM * N_CHAN +
                        FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
                       ELEMENT_PER_PACKET];
  AxiPacket out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET];
  AxiPacket packet;

  // stream in the first input  matrix
  // T ac[INPUT_DIM][INPUT_DIM][N_CHAN];
  // T wt[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT];
  // T out[OUTPUT_DIM][OUTPUT_DIM][N_FILT];

  int push_idx = 0;
LOOP_PUSH_AC:
  for (int i = 0; i < INPUT_DIM; i++) {
    for (int j = 0; j < INPUT_DIM; j++) {
      for (int k = 0; k < N_CHAN; k += ELEMENT_PER_PACKET) {
      LOOP_PUSH_AC_ASSIGN:
        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
          packet.data[m] = matOp1[i][j][k + m];
        }
        inp_stream[push_idx] = packet;
        push_idx++;
      }
    }
  }
LOOP_PUSH_WT:
  for (int i = 0; i < FILTER_DIM; i++) {
    for (int j = 0; j < FILTER_DIM; j++) {
      for (int k = 0; k < N_CHAN; k++) {
        for (int m = 0; m < N_FILT; m += ELEMENT_PER_PACKET) {
        LOOP_PUSH_WT_ASSIGN:
          for (int n = 0; n < ELEMENT_PER_PACKET; n++) {
            packet.data[n] = matOp2[i][j][k][m + n];
          }
          inp_stream[push_idx] = packet;
          push_idx++;
        }
      }
    }
  }

  // stream in the second input  matrix

  // call the DUT
  convo_mi_hw_wrapped(inp_stream, out_stream);

  // extract the output matrix from the out stream
  int pop_idx = 0;
LOOP_POP_OP:
  for (int i = 0; i < OUTPUT_DIM; i++) {
    for (int j = 0; j < OUTPUT_DIM; j++) {
      for (int k = 0; k < N_FILT; k += ELEMENT_PER_PACKET) {
        packet = out_stream[pop_idx];
        pop_idx++;
      LOOP_POP_OP_ASSIGN:
        for (int m = 0; m < ELEMENT_PER_PACKET; m++) {
          out_hw[i][j][k + m] = packet.data[m];
        }
      }
    }
  }

  /* reference Convolution */
  convo_ref_sw(matOp1, matOp2, out_sw);

  /** Output Result comparison */
  err = 0;
  for (int i = 0; (i < OUTPUT_DIM && !err); i++)
    for (int j = 0; (j < OUTPUT_DIM && !err); j++)
      for (int k = 0; (k < N_FILT && !err); k++)
        if (out_sw[i][j][k] != out_hw[i][j][k]) {
          //printf("%d %d %d : sw = %ld hw = %ld \n", i, j, k,
          //       (unsigned long long)out_sw[i][j][k],
          //       (unsigned long long)out_hw[i][j][k]);
          err++;
        }

  if (err == 0)
    printf("Matrixes identical ... Test successful!\r\n");
  else
    printf("Test failed!\r\n");

  //	return err;
  return 0;
}

#endif
