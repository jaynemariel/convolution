#include <cstdlib>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "verified_convo.h"

// evaluates V_i polynomial at location r.
// O(n)
Elem evaluate_V_i(int mi, int ni, Elem level_i[], Elem r[]) {
  Elem *Phase1_eval = (Elem *)calloc(ni, sizeof(Elem));
  int num_terms = 1;
  Phase1_eval[0] = 1;
  Elem temp;

  for (int i = 0; i < mi; i++) {
    for (int j = 0; j < num_terms; j++) {
      temp = Phase1_eval[j];
      Phase1_eval[j] = mod_mlt(temp, 1 + PRIME - r[i]);
      Phase1_eval[j + num_terms] = mod_mlt(temp, r[i]);
    }
    num_terms = num_terms << 1;
  }
  Elem ans = 0;
  for (int k = 0; k < ni; k++)
    ans = mod(ans + mod_mlt(level_i[k], Phase1_eval[k]));
  free(Phase1_eval);
  return ans;
}

/*FIXME*/
// don't need to multiply by one in sum
Elem evaluate_S(int mi, int W1, int W2, int K, int stride, Elem level_i[],
                Elem r[]) {
  Elem ans = 0;
  Elem x;
  for (int i = 0; i < K; i++) {
    for (int j = 0; j < W2; j++) {
      x = (i + j * stride) % W1;
      ans = mod(ans + Phase1(x * (W2 * K) + j * K + i, r, mi));
    }
  }
  return ans;
}

// Performs Extended Euclidean Algorithm
// Used for computing multiplicative inverses mod p
void extEuclideanAlg(Elem u, Elem *u1, Elem *u2, Elem *u3) {
  *u1 = 1;
  *u2 = 0;
  *u3 = u;
  Elem v1 = 0;
  Elem v2 = 1;
  Elem v3 = PRIME;
  Elem q;
  Elem t1;
  Elem t2;
  Elem t3;
  do {
    q = *u3 / v3;
    // t1 = *u1 + p - q * v1;
    // t2 = *u2 + p - q*v2;
    // t3 = *u3 + p - q*v3;
    t1 = mod((*u1) + PRIME - mod_mlt(q, v1));
    t2 = mod((*u2) + PRIME - mod_mlt(q, v2));
    t3 = mod((*u3) + PRIME - mod_mlt(q, v3));
    (*u1) = v1;
    (*u2) = v2;
    (*u3) = v3;
    v1 = t1;
    v2 = t2;
    v3 = t3;
  } while (v3 != 0 && v3 != PRIME);
}

// Computes the modular multiplicative inverse of a modulo m,
// using the extended Euclidean algorithm
// only works for p=2^61-1
Elem inv(Elem a) {
  Elem u1;
  Elem u2;
  Elem u3;
  extEuclideanAlg(a, &u1, &u2, &u3);
  if (u3 == 1)
    return mod(u1);
  else
    return 0;
}

Elem extrap(Elem vec[], Elem n, Elem r) {
  Elem result = 0;
  Elem mult = 1;
  for (Elem i = 0; i < n; i++) {
    mult = 1;
    for (Elem j = 0; j < n; j++) {
      if (i > j)
        mult = mod_mlt(mod_mlt(mult, mod(r - j + PRIME)), inv(i - j));
      if (i < j)
        mult =
            mod_mlt(mod_mlt(mult, mod(r - j + PRIME)), inv(mod(i + PRIME - j)));
    }
    result = mod(result + mod_mlt(mult, vec[i]));
  }
  return result;
}

void check_rounds(Elem a1, Elem a2, Elem F[][3], Elem r[], Elem d, Elem g) {
  if (a1 != mod(F[0][0] + F[0][1]))
    std::cout << "first check failed" << std::endl, exit(1);
  std::cout << "claim 0: " << a1 << std::endl; 
  Elem check;
  for (int i = 1; i < d; i++) {
    check = extrap(F[i - 1], g, r[i - 1]);
    std::cout << "claim " << i << ": " << check << std::endl; 
    if ((mod(F[i][0] + F[i][1]) != check) &&
        (mod(F[i][0] + F[i][1]) + PRIME != check))
      std::cout << "check " << i << " failed" << std::endl, exit(1);
  }

  check = extrap(F[d - 1], g, r[d - 1]);
  if (a2 != check)
    std::cout << "last check failed" << std::endl, exit(1);

  std::cout << "last claim: " << a2 << std::endl; 

}

// Verifies the 2-D convolutional layer with following parameters:
int main() {

  // Initialize input activation tensor.
  static Elem activation_tensor[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH];
  static Elem activation_tensor_orig[N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH];
  std::cout << "activation_tensor" << std::endl;
  for (int i = 0; i < N_CHAN * INPUT_DIM * INPUT_DIM * N_BATCH; i++) {
    activation_tensor[i] = rand() % 100;
    activation_tensor_orig[i] = activation_tensor[i];
    std::cout << activation_tensor[i] << ", ";
  }

  // Filters (also called kernels/weights).
  static Elem filters[N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT];
  static Elem filters_orig[N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT];
  std::cout << "\nfilters[i]"  << std::endl;
  for (int i = 0; i < N_CHAN * FILTER_DIM * FILTER_DIM * N_FILT; i++) {
    filters[i] = rand() % 100;
    filters_orig[i] = filters[i];
    std::cout << filters[i]  << ", ";
  }

  // Output values.
  static Elem output_tensor[OUTPUT_DIM * OUTPUT_DIM * N_FILT * N_BATCH];

  // Sum-check protocol random values for each iteration.
  // I call them v, u, t, x and y.
  // Should be picked from the field [0, F_p], but this picks from [0,
  // RAND_MAX].
  static Elem
      random_values[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN];
  std::cout << "\nrandom_values"  << std::endl;
  for (int i = 0; i < 2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN;
       i++) {
    random_values[i] = rand();
    std::cout << random_values[i]  << ", ";
  }

  // Random point q = (i, j, LOG_FILTER_DIM, LOG_N_BATCH) to evaluate the output
  // volume 'output_tensor'.
  static Elem random_q[2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH];
  std::cout << "\nrandom_q"  << std::endl;
  for (int i = 0; i < 2 * LOG_OUTPUT_DIM + LOG_N_FILT + LOG_N_BATCH; i++) {
    random_q[i] = rand();
    std::cout << random_q[i]  << ", ";
  }

std::cout << std::endl;

  // Polynomial extrapolations for each round.
  static Elem h_function[2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN]
                        [3];

  // Sum function values.
  // Sum 1 function s(x, i, u) = 1 if x = i + u and = 0 otherwise.
  static Elem sum_1_orig[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];
  // Sum 2 function s(y, j, v) = 1 if y = j + v and = 0 otherwise.
  static Elem sum_2_orig[INPUT_DIM * OUTPUT_DIM * FILTER_DIM];
  for (int i = 0; i < FILTER_DIM; i++) {
    for (int j = 0; j < OUTPUT_DIM; j++) {
      int x = (i + j * STRIDE) % INPUT_DIM;
      sum_1_orig[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
      sum_2_orig[x * (OUTPUT_DIM * FILTER_DIM) + j * FILTER_DIM + i] = 1;
    }
  }

// print out initial values


  clock_t t = clock();
  // Local computation using field arithmetic.
  for (int i = 0; i < OUTPUT_DIM; i++) {
    for (int j = 0; j < OUTPUT_DIM; j++) {
      // Apply filters, wrap activation_tensor around with
      // (i+LOG_FILTER_DIM)%INPUT_DIM
      for (int k = 0; k < N_FILT; k++) {
        for (int l = 0; l < N_BATCH; l++) {
          for (int t = 0; t < N_CHAN; t++) {
            for (int u = 0; u < FILTER_DIM; u++) {
              for (int v = 0; v < FILTER_DIM; v++) {
                output_tensor[l * OUTPUT_DIM * OUTPUT_DIM * N_FILT +
                              OUTPUT_DIM * OUTPUT_DIM * k + OUTPUT_DIM * i +
                              j] =
                    mod(output_tensor[l * OUTPUT_DIM * OUTPUT_DIM * N_FILT +
                                      OUTPUT_DIM * OUTPUT_DIM * k +
                                      OUTPUT_DIM * i + j] +
                        mod_mlt(
                            filters[FILTER_DIM * FILTER_DIM * N_CHAN * k +
                                    FILTER_DIM * FILTER_DIM * t +
                                    FILTER_DIM * u + v],
                            activation_tensor
                                [l * INPUT_DIM * INPUT_DIM * N_CHAN +
                                 ((STRIDE * i + u) % INPUT_DIM) * INPUT_DIM *
                                     N_CHAN +
                                 ((STRIDE * j + v) % INPUT_DIM) * N_CHAN + t]));
              }
            }
          }
        }
      }
    }
  }
  t = clock() - t;
  std::cout << (double)t / CLOCKS_PER_SEC
            << ": time of unverified covolution using field arithmetic.\n";

  // reset output_tensor
  for (int i = 0; i < OUTPUT_DIM * OUTPUT_DIM * N_FILT * N_BATCH; i++)
    output_tensor[i] = 0;

  // Local computation without using field arithmetic.
  t = clock();
  for (int i = 0; i < OUTPUT_DIM; i++) {
    for (int j = 0; j < OUTPUT_DIM; j++) {
      for (int k = 0; k < N_FILT; k++) {
        for (int l = 0; l < N_BATCH; l++) {
          for (int t = 0; t < N_CHAN; t++) {
            for (int u = 0; u < FILTER_DIM; u++) {
              for (int v = 0; v < FILTER_DIM; v++) {
                output_tensor[l * OUTPUT_DIM * OUTPUT_DIM * N_FILT +
                              OUTPUT_DIM * OUTPUT_DIM * k + OUTPUT_DIM * i +
                              j] =
                    output_tensor[l * OUTPUT_DIM * OUTPUT_DIM * N_FILT +
                                  OUTPUT_DIM * OUTPUT_DIM * k + OUTPUT_DIM * i +
                                  j] +
                    filters[FILTER_DIM * FILTER_DIM * N_CHAN * k +
                            FILTER_DIM * FILTER_DIM * t + FILTER_DIM * u + v] *
                        (activation_tensor[l * INPUT_DIM * INPUT_DIM * N_CHAN +
                                           ((STRIDE * i + u) % INPUT_DIM) *
                                               INPUT_DIM * N_CHAN +
                                           ((STRIDE * j + v) % INPUT_DIM) *
                                               N_CHAN +
                                           t]);
              }
            }
          }
        }
      }
    }
  }
  t = clock() - t;
  std::cout << (double)t / CLOCKS_PER_SEC
            << ": time of unverified convolution without field arithmetic.\n";

  // print output_tensor
  std::cout << "\noutput_tensor" << std::endl;
  for (int i = 0; i < OUTPUT_DIM * OUTPUT_DIM * N_FILT * N_BATCH; i++)
    std::cout << output_tensor[i] << std::endl;

  t = clock();
  // Verifier evaluates a random point in the MLE of the returned volume
  Elem a1 = evaluate_V_i(LOG_OUTPUT_DIM * 2 + LOG_N_FILT + LOG_N_BATCH,
                         OUTPUT_DIM * OUTPUT_DIM * N_FILT * N_BATCH,
                         output_tensor, random_q);
  t = clock() - t;
  std::cout << (double)t / CLOCKS_PER_SEC
            << ": time of evaluation by verifier.\n";

  // For intermediate layers, this claim is returned by the prover. For input,
  // the verifier computes this

  t = clock();
  for (int round = 0;
       round < 2 * LOG_FILTER_DIM + LOG_N_CHAN + 2 * LOG_INPUT_DIM; round++) {
    convo_sum_check_prover_hw(round, activation_tensor, filters, random_values,
                              random_q, h_function);
  }
  t = clock() - t;
  std::cout << (double)t / CLOCKS_PER_SEC
            << ": time of sum checked convolution.\n";

  static Elem z1[LOG_N_CHAN + 2 * LOG_INPUT_DIM + LOG_N_BATCH];
  static Elem z2[2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_N_FILT];
  static Elem z3[LOG_INPUT_DIM + LOG_OUTPUT_DIM + LOG_FILTER_DIM];
  static Elem z4[LOG_INPUT_DIM + LOG_OUTPUT_DIM + LOG_FILTER_DIM];

  // activation_tensor
  // t y x LOG_N_BATCH
  for (int i = 0; i < LOG_N_CHAN + 2 * LOG_INPUT_DIM; i++)
    z1[i] = random_values[i + 2 * LOG_FILTER_DIM];
  for (int i = 0; i < LOG_N_BATCH; i++)
    z1[i + LOG_N_CHAN + 2 * LOG_INPUT_DIM] =
        random_q[i + 2 * LOG_OUTPUT_DIM + LOG_N_FILT];

  // filters
  // v u t LOG_FILTER_DIM
  for (int i = 0; i < 2 * LOG_FILTER_DIM + LOG_N_CHAN; i++)
    z2[i] = random_values[i];
  for (int i = 0; i < LOG_N_FILT; i++)
    z2[i + 2 * LOG_FILTER_DIM + LOG_N_CHAN] = random_q[i + 2 * LOG_OUTPUT_DIM];

  // sum_1
  // v j y
  for (int i = 0; i < LOG_FILTER_DIM; i++)
    z3[i] = random_values[i];
  for (int i = 0; i < LOG_OUTPUT_DIM; i++)
    z3[i + LOG_FILTER_DIM] = random_q[i];
  for (int i = 0; i < LOG_INPUT_DIM; i++)
    z3[i + LOG_FILTER_DIM + LOG_OUTPUT_DIM] =
        random_values[i + 2 * LOG_FILTER_DIM + LOG_N_CHAN];

  // sum_2
  // u i x
  for (int i = 0; i < LOG_FILTER_DIM; i++)
    z4[i] = random_values[i + LOG_FILTER_DIM];
  for (int i = 0; i < LOG_OUTPUT_DIM; i++)
    z4[i + LOG_FILTER_DIM] = random_q[i + LOG_OUTPUT_DIM];
  for (int i = 0; i < LOG_INPUT_DIM; i++)
    z4[i + LOG_FILTER_DIM + LOG_OUTPUT_DIM] =
        random_values[i + 2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_INPUT_DIM];

  t = clock();
  Elem claim = evaluate_V_i(2 * LOG_INPUT_DIM + LOG_N_CHAN + LOG_N_BATCH,
                            INPUT_DIM * INPUT_DIM * N_CHAN * N_BATCH,
                            activation_tensor_orig, z1);
  t = clock() - t;
  //std::cout << (double)t / CLOCKS_PER_SEC
 //           << ": time of second evaluation (?)\n";

  t = clock();
  Elem Meval =
      evaluate_V_i(2 * LOG_FILTER_DIM + LOG_N_CHAN + LOG_N_FILT,
                   FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT, filters_orig, z2);
  Elem Seval1 =
      evaluate_S(LOG_FILTER_DIM + LOG_INPUT_DIM + LOG_OUTPUT_DIM, INPUT_DIM,
                 OUTPUT_DIM, FILTER_DIM, STRIDE, sum_1_orig, z3);
  Elem Seval2 =
      evaluate_S(LOG_FILTER_DIM + LOG_INPUT_DIM + LOG_OUTPUT_DIM, INPUT_DIM,
                 OUTPUT_DIM, FILTER_DIM, STRIDE, sum_2_orig, z4);
  Elem a2 = mod_mlt(mod_mlt(claim, Meval), mod_mlt(Seval1, Seval2));

  check_rounds(a1, a2, h_function, random_values,
               2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN, 3);
  t = clock() - t;
  std::cout << (double)t / CLOCKS_PER_SEC << ": time of verifier.\n";

  // print_values(a1, a2, h_function, random_values,
  //              2 * LOG_FILTER_DIM + 2 * LOG_INPUT_DIM + LOG_N_CHAN, 3);

  std::cout << "TEST PASSED: Computation was verified!!\n";

  return 0;
}
