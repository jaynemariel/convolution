#include <ap_axi_sdata.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////
// PARAMETERS.
//////////////////////////////////////////////////////////////////////

///////////////////// Test constants ///////////////////////////
#ifdef QUICK_TEST

// #define INPUT_DIM   5
// #define FILTER_DIM  2
// #define OUTPUT_DIM  4

#define INPUT_DIM 4
#define N_CHAN 2
#define N_FILT 2
#define FILTER_DIM 2
#define OUTPUT_DIM 3

#define ELEMENT_BIT_SIZE 64
#define WEIGHT_ELEMENT_BIT_SIZE 8
#define ELEMENT_PER_PACKET 1

#endif
//////////////// end of test constants //////////////////////

// These are only for the reference implementation.
#define STRIDE_HEIGHT 1
#define STRIDE_WIDTH 1
#define PAD_TOP 0
#define PAD_LEFT 0

// Matrix element type.
// We always use unsigned int.
typedef ap_uint<ELEMENT_BIT_SIZE> Elem;
typedef ap_uint<WEIGHT_ELEMENT_BIT_SIZE> Wt_Elem;

// AXI packet interface.
struct AxiPacket {
  Elem data[ELEMENT_PER_PACKET];
};


#define MAX(a, b) (((a) > (b)) ? (a) : (b))
// Largest dimension.
#define MAX_DIM MAX(MAX(FILTER_DIM, INPUT_DIM), MAX(N_CHAN, OUTPUT_DIM))

// We always use unsigned int as element type.
// The bitsize must be passed via ELEM_BIT_SIZE.
/* typedef ap_uint<ELEMENT_BIT_SIZE> Elem; */
//typedef unsigned long long Elem; // u64

//////////////////////////////////////////////////////////////////////
// Inlined functions.
//////////////////////////////////////////////////////////////////////

#define MASK 4294967295           // 2^32-1
#define PRIME 2305843009213693951 // 2^61-1

// Efficient modular arithmetic function for p=2^61-1. Only works for this value
// of p. This function might return a number slightly greater than p (possibly
// by an additive factor of 8); It'd be cleaner to check if the answer is
// greater than p and if so subtract p, but I don't want to pay that efficiency
// hit here, so the user should just be aware of this.
inline Elem mod(Elem x) { return (x >> 61) + (x & PRIME); }

// Efficient modular multiplication function mod 2^61-1.
inline Elem mod_mlt(Elem x, Elem y) {
  Elem hi_x = x >> 32;
  Elem hi_y = y >> 32;
  Elem low_x = x & MASK;
  Elem low_y = y & MASK;

  // Since mod might return something slightly large than 2^61-1,
  // we need to multiply by 8 in two pieces to avoid overflow.
  Elem piece1 = mod((hi_x * hi_y) << 3);
  Elem z = (hi_x * low_y + hi_y * low_x);
  Elem hi_z = z >> 32;
  Elem low_z = z & MASK;

  // Note 2^64 mod (2^61-1) is 8.
  Elem piece2 = mod((hi_z << 3) + mod((low_z << 32)));
  Elem piece3 = mod(low_x * low_y);
  Elem result = mod(piece1 + piece2 + piece3);

  return result;
}


 
//////////////////////////////////////////////////////////////////////
// Function prototypes
//////////////////////////////////////////////////////////////////////

void convo_mi_hw_wrapped(
    AxiPacket in_stream[(INPUT_DIM * INPUT_DIM * N_CHAN)/ ELEMENT_PER_PACKET +
                         (FILTER_DIM * FILTER_DIM * N_CHAN * N_FILT) /
                        ELEMENT_PER_PACKET],
    AxiPacket
        out_stream[(OUTPUT_DIM * OUTPUT_DIM * N_FILT) / ELEMENT_PER_PACKET]);

void convo_mi_hw(Elem in[INPUT_DIM][INPUT_DIM][N_CHAN],
                 Wt_Elem weights[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                 Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT]);

void convo_mi_mod_hw(Elem in[INPUT_DIM][INPUT_DIM][N_CHAN],
                     Wt_Elem weights[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                     Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT]);

void convo_ref_hw(Elem data[INPUT_DIM][INPUT_DIM][N_CHAN],
                  Wt_Elem weights[FILTER_DIM][FILTER_DIM][N_CHAN][N_FILT],
                  Elem out[OUTPUT_DIM][OUTPUT_DIM][N_FILT],
                  Elem biases[N_FILT]);



