#!/usr/bin/env python

import os

import config

def get_report_path(dirnames_to_config, dirname):
    c = dirnames_to_config[dirname]
    report_name = c['topFn']
    if c['wrapped']:
        report_name = report_name + '_wrapped'
    report_path = os.path.join('$(SYNTH_DIR)', dirname,
                               'solution/syn/report/' + report_name + '_csynth.xml')
    return report_path

def generate_makefile_rules():
    hardware_to_dirname_to_config = config.get_hardware_to_dirname_to_config_map()
    for hardware in sorted(hardware_to_dirname_to_config):
        dirnames_to_config = hardware_to_dirname_to_config[hardware]
        print '### Targets for:', hardware
        print

        # Synthesis rules.
        for dirname in dirnames_to_config:
            # Get the exact report path.
            report_path = get_report_path(dirnames_to_config, dirname)
            print report_path + ':'
            print '\t./scripts/run_hls.py ' + dirname
            print

        # Synth all and CSV generation rule.
        csv_out = '$(CSV_DIR)/' + hardware + '-results.csv'
        print csv_out + ': \\'
        for dirname in dirnames_to_config:
            report_path = get_report_path(dirnames_to_config, dirname)
            print ' ' + report_path + ' \\'
        # print ' python-environment | $(CSV_DIR)'
        print ' scripts/reports_to_csv.py | $(CSV_DIR)'
        print '\t@$(PYTHON) scripts/reports_to_csv.py ' + hardware
        print

        # make mmult, make convo, etc.
        print hardware + ':', csv_out
        print '\t@echo "----- 8< ----- CSV ----- 8< -----"'
        print '\t@cat $<'
        print '\t@echo "----- >8 --------------- >8 -----"'
        print

        # make mmult-print-partial, make convo-print-partial, etc.
        print hardware + '-print-partial: scripts/reports_to_csv.py | $(CSV_DIR)'
	print '\t@$(PYTHON) scripts/reports_to_csv.py ' + hardware
	print '\t@echo "----- 8< ----- CSV ----- 8< -----"'
	print '\t@cat $(CSV_DIR)/' + hardware + '-results.csv'
	print '\t@echo "----- >8 --------------- >8 -----"'

        # Plot generation rule.
        # print 'plot-' + hardware + ':', synth_all_rule
        # print '\t@$(PYTHON) scripts/plot.py ' + hardware
        # print

generate_makefile_rules()
