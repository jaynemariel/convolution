# Runs Vivado HLS for different hardware (mmult, convo, prover, ...).
#
# Don't run this script directly! Use the Makefile instead, which sets the
# necessary environment variables.
#
# Open synthesized configurations in GUI for analysis with:
#
#   $ vivado_hls -p results/<function>_<config> &

# The script expects the following environment variables:
# The "hardware" env variable selects the code for synthesis (e.g., mmult /
# convo / prover). See config.py for more details.
set expected_env_vars {
    hardware
    part
    clock
    topFn
    wrapped
    optLevel
    parallelism
    inputDim
    elemBitSize
    weightelemBitSize
    elemInPkt
}
puts "### Generic options:"
foreach env_var $expected_env_vars {
    puts [format "  %-16s: %16s" $env_var $env($env_var)]
}

### Convolution options.
# Note that inputDim is always passed in $env(inputDim)
array set convo_opt {
    logInputDim    0

    channelNum     0
    logChannelNum  0

    filterNum      0
    logFilterNum   0

    filterDim      0
    logFilterDim   0

    batchNum       0
    logBatchNum    0

    outputDim      0
    logOutputDim   0

    filterBurstNum 0
}

proc log_2 num {
    expr { log($num) / [expr log(2)] }
}

proc log_2_ceil num {
    expr int(ceil([log_2 $num]))
}

proc log_2_with_power_of_2_check num {
    set val [log_2 $num]
    if { $val != floor($val) } {
        error "$num is not power of two!!!"
    }
    return [expr int($val)]
}

proc get_output_dim {inputDim filterDim stride} {
    set logOutDim [log_2_ceil [expr {($inputDim-$filterDim)/(1+$stride)}]]
    return [expr 2 ** $logOutDim]
}

if { $env(hardware) == "convo" || $env(hardware) == "verified_convo"} {
    set convo_opt(logInputDim) [log_2_with_power_of_2_check $env(inputDim)]

    set convo_opt(channelNum) $env(channelNum)
    set convo_opt(logChannelNum) [log_2_with_power_of_2_check $convo_opt(channelNum)]

    set convo_opt(filterNum) $env(filterNum)
    set convo_opt(logFilterNum) [log_2_with_power_of_2_check $convo_opt(filterNum)]

    set convo_opt(filterDim) $env(filterDim)
    set convo_opt(logFilterDim) [log_2_with_power_of_2_check $convo_opt(filterDim)]

    set convo_opt(batchNum) $env(batchNum)
    set convo_opt(logBatchNum) [log_2_with_power_of_2_check $convo_opt(batchNum)]

    set convo_opt(outputDim) [get_output_dim $env(inputDim) $convo_opt(filterDim) 1]
    set convo_opt(logOutputDim) [log_2_with_power_of_2_check $convo_opt(outputDim)]

    set convo_opt(filterBurstNum) $env(filterBurstNum)

#    set convo_opt(dim) [ expr {$convo_opt(outputDim)*$convo_opt(outputDim)*$env(filterNum)}]
#    set convo_opt(logDim) [expr {$convo_opt(logOutputDim)*2 + $convo_opt(logFilterNum)}]

    set convo_opt(dim) [ expr {$env(inputDim)*$env(inputDim)*$env(channelNum)}]
    set convo_opt(logDim) [expr {$convo_opt(logInputDim)*2 + $convo_opt(logChannelNum)}]

    puts "### Convolution options:"
    foreach o [lsort [array names convo_opt]] {
        puts [format "  %-16s: %16s" $o $convo_opt($o)]
    }
}

### Script options.
array set opt {
    doCSim                  1
    doSynth                 0
    doCosim                 0
    doExport                0
}
puts "### Checking script option overrides by environment variables:"
foreach o [lsort [array names opt]] {
    if {[info exists env($o)]} {
        puts "  Setting $o to: [set opt($o) $env($o)]"
    }
}

# The Makefile invokes this script from a specific working directory, so we use
# the "current directory" as project directory.
open_project . -reset

# If "wrapped" is true, we add a "_wrapped" postfix to the top level function.
if {$env(wrapped) == 0} {
    set_top $env(topFn)
} elseif {$env(wrapped) == 1} {
    set_top $env(topFn)_wrapped
}

set defines ""

append defines " -DWRAPPED=$env(wrapped)"
append defines " -DELEMENT_BIT_SIZE=$env(elemBitSize)"
append defines " -DWEIGHT_ELEMENT_BIT_SIZE=$env(weightelemBitSize)"
append defines " -DELEMENT_PER_PACKET=$env(elemInPkt)"

append defines " -DINPUT_DIM=$env(inputDim)"

if { $env(hardware) == "convo" || $env(hardware) == "verified_convo"} {
    append defines " -DLOG_INPUT_DIM=$convo_opt(logInputDim)"

    append defines " -DN_CHAN=$convo_opt(channelNum)"
    append defines " -DLOG_N_CHAN=$convo_opt(logChannelNum)"

    append defines " -DN_FILT=$convo_opt(filterNum)"
    append defines " -DLOG_N_FILT=$convo_opt(logFilterNum)"

    append defines " -DFILTER_DIM=$convo_opt(filterDim)"
    append defines " -DLOG_FILTER_DIM=$convo_opt(logFilterDim)"

    append defines " -DN_BATCH=$convo_opt(batchNum)"
    append defines " -DLOG_N_BATCH=$convo_opt(logBatchNum)"

    append defines " -DOUTPUT_DIM=$convo_opt(outputDim)"
    append defines " -DLOG_OUTPUT_DIM=$convo_opt(logOutputDim)"

    append defines " -DFILTER_BURST_NUM=$convo_opt(filterBurstNum)"

    append defines " -DDIM=$convo_opt(dim)"
    append defines " -DLOG_DIM=$convo_opt(logDim)"
}

add_files     ../../src/$env(hardware).cpp       -cflags "$defines"
add_files -tb ../../src/$env(hardware)_test.cpp  -cflags "$defines"

open_solution "solution"

set_part $env(part)
create_clock -period $env(clock) -name default

# Different optimization levels for the different functions.

if {$env(hardware) == "mmult"} {
    if {$env(optLevel) == 0} {
        # No optimization!
    } elseif {$env(optLevel) == 1} {
        set_directive_pipeline -II 1 "mmult_hw/L2"
    } elseif {$env(optLevel) == 2} {
        set_directive_inline "mmult_hw"
        set_directive_pipeline -II 1 "mmult_hw/L2"
        set_directive_array_partition -type block -factor 16 -dim 2 "mmult_hw" a
        set_directive_array_partition -type block -factor 16 -dim 1 "mmult_hw" b
    }
} elseif {$env(hardware) == "convo" && $env(topFn) == "convo_mi_hw" } {
    if {$env(optLevel) == 0} {
        # TODO!
    } elseif {$env(optLevel) == 2} {

#TODO: set directives for wrapped version!!! This only works for unwrapped
#        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_N_FILT"
#        set_directive_array_partition -type cyclic -factor [expr $env(channelNum)/2]       -dim 3 "convo_mi_hw" ac
#        set_directive_array_partition -type cyclic -factor [expr $convo_opt(channelNum)/2] -dim 3 "convo_mi_hw" wt
    } elseif {$env(optLevel) == 3} {
#TODO: check if pipeline Loop Chan is redundant
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_CHAN"
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_N_FILT"
        set_directive_array_partition -type block -factor 16  -dim 1 "convo_mi_hw" sum
        
        set_directive_loop_merge "convo_mi_hw/LOOP_INPUT_COL"
        set_directive_unroll -factor 2 "convo_mi_hw/LOOP_N_FILT"
        #set_directive_array_partition -type cyclic -factor 2 -dim 4 "convo_mi_hw" wt
        set_directive_array_partition -type cyclic -factor [expr $env(channelNum)/2]       -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type cyclic -factor [expr $env(channelNum)/2] -dim 3 "convo_mi_hw" wt
    } elseif {$env(optLevel) == 1} {
# Unconstrained memory model
if {$env(parallelism) < $env(channelNum)} {
# Set directives when parallelism != Channel 
        set_directive_unroll -factor [expr $env(parallelism)] "convo_mi_hw/LOOP_CHAN"
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_CHAN"
        #set_directive_array_partition -type cyclic -factor 64 -dim 1 "convo_mi_hw" sum
        set_directive_array_partition -type complete "convo_mi_hw" sum
        set_directive_array_partition -type cyclic -factor [expr $env(parallelism)]   -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type cyclic -factor [expr $env(parallelism)]   -dim 3 "convo_mi_hw" wt
        set_directive_array_partition -type cyclic -factor [expr $env(parallelism)]   -dim 3 "convo_mi_hw" out
} else {
# Set directives when parallelism = Channel, L1 and L2
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_N_FILT"
        set_directive_array_partition -type complete "convo_mi_hw" sum
        set_directive_array_partition -type complete -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type complete -dim 3 "convo_mi_hw" wt
}
# Directives for wrapper
        set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOPx_IN_AC_ST"
        set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOPx_IN_WT_ST"
        set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOPx_OUT_OP_ST"
    }
} elseif {$env(hardware) == "convo" && $env(topFn) == "convo_mi_mod_hw"} {
    if {$env(optLevel) == 0} {
        # No Optimization!
    } elseif {$env(optLevel) == 1} {
# Set directives for wrapped version!!! This only works for unwrapped
        set_directive_unroll -factor [expr $env(channelNum)/16] "convo_mi_mod_hw/LOOP_CHAN"
        set_directive_pipeline -II 1 "convo_mi_mod_hw/LOOP_CHAN"
        set_directive_array_partition -type complete "convo_mi_mod_hw" sum
        set_directive_array_partition -type cyclic -factor [expr $env(channelNum)/16] -dim 3 "convo_mi_mod_hw" ac
        set_directive_array_partition -type cyclic -factor [expr $env(channelNum)/16] -dim 3 "convo_mi_mod_hw" wt
    } elseif {$env(optLevel) == 2} {
        # TODO!
    }
} elseif {$env(hardware) == "verified_convo" && $env(topFn) == "convo_sum_check_prover_hw"} {
    if {$env(optLevel) == 0} {
        # No Optimization!
    } elseif {$env(optLevel) == 1} {
# Base improvements
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var1_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var2_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var3_t"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var4_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var5_x"
        set_directive_pipeline -II 1 "update_sum/UPDATE_SUM_1_1_1"
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" temp
        set_directive_unroll -factor 2 "convo_sum_check_prover_hw/INIT_FILTERS_Kernels"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/INIT_FILTERS_Kernels"
        set_directive_array_partition -type block -factor 10 -dim 1 "convo_sum_check_prover_hw" activation_tensor
    } elseif {$env(optLevel) == 2} {
# Not working!!! 
        set_directive_unroll -factor 4 "convo_sum_check_prover_hw/R_var1_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var1_v"
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" filters
        #set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" activation_tensor
        set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" sum_1
        #set_directive_array_partition -type complete "convo_sum_check_prover_hw" sum_2
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" temp
    }
} elseif {$env(hardware) == "verified_convo" && $env(topFn) == "convo_sum_check_prover_small_hw"} {
    if {$env(optLevel) == 0} {
        # No Optimization!
    } elseif {$env(optLevel) == 1} {
# Base improvements
        set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/R_var1_t"
        set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/R_var2_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/R_var3_t"
        set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/R_var4_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/R_var5_x"
        set_directive_pipeline -II 1 "update_sum/UPDATE_SUM_1_1_1"
        set_directive_array_partition -type complete "convo_sum_check_prover_small_hw" temp
        set_directive_unroll -factor 2 "convo_sum_check_prover_small_hw/INIT_FILTERS_Cols"
        set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/INIT_FILTERS_Cols"
        #set_directive_unroll -factor 2 "convo_sum_check_prover_small_hw/INIT_FILTERS_Kernels"
        #set_directive_pipeline -II 1 "convo_sum_check_prover_small_hw/INIT_FILTERS_Kernels"
        set_directive_array_partition -type block -factor 10 -dim 1 "convo_sum_check_prover_small_hw" activation_tensor
    } elseif {$env(optLevel) == 2} {
# Not working!!! 
        set_directive_unroll -factor 4 "convo_sum_check_prover_hw/R_var1_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var1_v"
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" filters
        #set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" activation_tensor
        set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" sum_1
        #set_directive_array_partition -type complete "convo_sum_check_prover_hw" sum_2
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" temp
    }
} elseif {$env(hardware) == "verified_convo" && $env(topFn) == "convo_sum_check_prover_quad_hw"} {
    if {$env(optLevel) == 0} {
        # No Optimization!
        #set_directive_inline  "convo_sum_check_prover_quad_hw/sum_check_activation_hw"
        set_directive_unroll "sum_check_activation_hw/LOOP_QUAD_ROUNDS_WRITE_F"
        set_directive_pipeline -II 1 "sum_check_activation_hw/LOOP_QUAD_ROUNDS_WRITE_F"
    } elseif {$env(optLevel) == 1} {
# Base improvements for quadratic activations prover
        #set_directive_unroll -factor 2 "sum_check_activation_hw/LOOP_QUAD_INIT_I_1"
        set_directive_pipeline -II 1 "sum_check_activation_hw/LOOP_QUAD_INIT_I_1"
        #set_directive_unroll -factor 2 "sum_check_activation_hw/LOOP_QUAD_ROUNDS_ACCUM"
        set_directive_pipeline -II 1 "sum_check_activation_hw/LOOP_QUAD_ROUNDS_ACCUM"

        set_directive_unroll "sum_check_activation_hw/LOOP_QUAD_ROUNDS_WRITE_F"
        set_directive_pipeline -II 1 "sum_check_activation_hw/LOOP_QUAD_ROUNDS_WRITE_F"

        set_directive_array_partition -type cyclic -factor 32 -dim 1 "sum_check_activation_hw" V_quad
        set_directive_array_partition -type cyclic -factor 32 -dim 1 "sum_check_activation_hw" I_quad



# Set directives for convo prover
        set_directive_pipeline -II 1 "convo_sum_check_prover_quad_hw/R_var1_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_quad_hw/R_var2_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_quad_hw/R_var3_t"
        set_directive_pipeline -II 1 "convo_sum_check_prover_quad_hw/R_var4_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_quad_hw/R_var5_x"
        set_directive_pipeline -II 1 "update_sum/UPDATE_SUM_1_1_1"
        set_directive_array_partition -type complete "convo_sum_check_prover_quad_hw" temp
        set_directive_unroll -factor 2 "convo_sum_check_prover_quad_hw/INIT_FILTERS_Kernels"
        set_directive_pipeline -II 1 "convo_sum_check_prover_quad_hw/INIT_FILTERS_Kernels"
        set_directive_array_partition -type block -factor 10 -dim 1 "convo_sum_check_prover_quad_hw" activation_tensor
    } elseif {$env(optLevel) == 2} {
# Not working!!! 
        #set_directive_unroll -factor 4 "convo_sum_check_prover_hw/R_var1_y"
        #set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var1_v"
        #set_directive_array_partition -type complete "convo_sum_check_prover_hw" filters
        ##set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" activation_tensor
        #set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" sum_1
        ##set_directive_array_partition -type complete "convo_sum_check_prover_hw" sum_2
        #set_directive_array_partition -type complete "convo_sum_check_prover_hw" temp
    }
}





if {$opt(doCSim)} {
    puts "\n"
    puts "************************************************************************"
    puts "* Running csim_design to validate C code is functional"
    puts "************************************************************************"
    puts "\n"
    csim_design -clean
}

if {$opt(doSynth)} {
    puts "\n"
    puts "************************************************************************"
    puts "*  Running csynth_design to create core RTL"
    puts "************************************************************************"
    puts "\n"
    csynth_design
}

if {$opt(doCosim)} {
    puts "\n"
    puts "************************************************************************"
    puts "*  Running cosim_design to verify RTL"
    puts "************************************************************************"
    puts "\n"
    cosim_design
}

if {$opt(doExport)} {
    puts "\n"
    puts "*************************************************************************"
    puts "*  Running export_design to generate IP package"
    puts "*************************************************************************"
    puts "\n"
    export_design
}

close_project
exit
