#!/usr/bin/env python

import os
import sys
import glob
import xmltodict
import csv
# import numpy as np
import pandas as pd
# Set Non-GUI backend for matplotlib.
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

hardware_name = sys.argv[1]
csv_file = 'csvs/' + hardware_name + '-results.csv'

def plot_single_thing():
    # data = pd.read_csv(csv_file, index_col = 0)  # "Config" column as index.
    # data = data.sort_index() # Sort the data.

    data = pd.read_csv(csv_file)
    data = data.drop('ConfigName', axis = 1)

    # Filter some stuff out:
    # data = data[data.elemInPkt == 1]
    # data = data[data.wrapped == 1]

    x='inputDim'
    y='LUT'
    hue='optLevel'

    sns.pointplot(x=x, y=y, hue=hue, data=data);
    plt.show()

    # fig.savefig('results.png', bbox_inches='tight')
    # print 'results.png'


def plot_more_things():
    data = pd.read_csv(csv_file)
    data = data.drop('ConfigName', axis = 1)

    # Filter some stuff out:
    # data = data[data.elemInPkt == 1]
    # data = data[data.wrapped == 1]

    x='inputDim'

    # y='LUT'
    # y='DSP48E'
    # y='BRAM_18K'
    y='Cycles'

    # hue='optLevel'
    hue='wrapped'

    # row='wrapped'
    row='optLevel'

    col='elemInPkt'

    sns.catplot(x=x, y=y, hue=hue, row=row, col=col, data=data, kind='point');

    # grid = sns.FacetGrid(data=data, row='inputDim', col='wrapped')
    # grid = grid.map(plt.plot, 'BRAM_18K')

    plt.show()

    # fig.savefig('results.png', bbox_inches='tight')
    # print 'results.png'


def plot_multiple_things():
    data = pd.read_csv('results.csv', index_col = 0)  # "Config" column as index.
    data = data.sort_index() # Sort the data.


    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1, figsize=(8, 5), sharex=True)
    sns.barplot(data.index, y='Cycles',   data=data, ax=ax1)
    sns.barplot(data.index, y='LUT',      data=data, ax=ax2)
    sns.barplot(data.index, y='FF',       data=data, ax=ax3)
    sns.barplot(data.index, y='BRAM_18K', data=data, ax=ax4)
    sns.barplot(data.index, y='DSP48E',   data=data, ax=ax5)

    ax5.set_xticklabels(ax5.get_xticklabels(), rotation=90)

    fig.savefig('results.png', bbox_inches='tight')
    print 'results.png'

def plot_overhead():
    ### Style?
    # darkgrid, whitegrid, dark, white, and ticks.
    # sns.set_style("white")
    # sns.set_style("whitegrid")
    # sns.set_style("darkgrid")
    # sns.set_style("ticks", {"xtick.major.size": 6, "ytick.major.size": 6})

    data = pd.read_csv('results.csv', index_col = 0)  # "Config" column as index.
    data = data.sort_index() # Sort the data.

    standalone_results = data[data.index.str.contains('wrapped_0')]
    standalone_results.index = standalone_results.index.map(
        lambda x: x.replace('--wrapped_0', '')
    )
    wrapped_results = data[data.index.str.contains('wrapped_1')]
    wrapped_results.index = wrapped_results.index.map(
        lambda x: x.replace('--wrapped_1', '')
    )

    overhead = 100*((wrapped_results/standalone_results)-1)
    # print overhead


    ### Multiple ax.
    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, 1, figsize=(8, 5), sharex=True)

    sns.barplot(x=overhead.index, y='Cycles',   data=overhead, ax=ax1)
    sns.barplot(x=overhead.index, y='LUT',      data=overhead, ax=ax2)
    sns.barplot(x=overhead.index, y='FF',       data=overhead, ax=ax3)
    sns.barplot(x=overhead.index, y='BRAM_18K', data=overhead, ax=ax4)
    sns.barplot(x=overhead.index, y='DSP48E',   data=overhead, ax=ax5)

    ax5.set_xticklabels(ax5.get_xticklabels(), rotation=90)


    fig.savefig('results.png', bbox_inches='tight')
    print 'results.png'
    # plt.show()

# write_csv()
# plot_single_thing()
plot_more_things()
# plot_multiple_things()
# plot_overhead()

