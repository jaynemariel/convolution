#!/usr/bin/env python

from itertools import product

#!/usr/bin/python

# This is a wrapper script around the run_hls TCL file.
#
# It looks up the configuration represented by the output directory name, and exports all options as environment variables, that the TCL file we read.

import os
import sys
import subprocess

import config

print '================ Running run_hls.py script ================'

# Create and go inside the output directory.
dir_to_make = sys.argv[1]
work_dir = os.path.join('results', dir_to_make)
if not os.path.exists(work_dir):
    os.makedirs(work_dir)
os.chdir(work_dir)

# Find config and export the config to the environment.
hardware_to_dirname_to_config = config.get_hardware_to_dirname_to_config_map()
for hardware in hardware_to_dirname_to_config:
    dirname_to_config = hardware_to_dirname_to_config[hardware]
    if dir_to_make in dirname_to_config:
        config = dirname_to_config[dir_to_make]

for opt in config:
    print 'Exporting', opt + ':', config[opt]
    os.environ[opt] = str(config[opt])

# Run the TCL file.
subprocess.call(['vivado_hls', '-f', '../../scripts/run_hls.tcl'])
