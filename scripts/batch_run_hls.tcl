##############################################
# Project settings
#  i 7     i 8     i 9     i 10     
#  fmm 2   fmm 3   fmm 4   fmm 11   
#  rf 64   rf 128  rf 256  rf 256   
#  rp2 4   rp2 4   rp2 4   rp2 4   
#  rf2 4   rf2 4   rf2 4   rf2 4   
#  

set version "Wed" 
set hardware "convo"
set topFn "convo_mi_hw"
#set hardware "verified_convo"
#set topFn "convo_sum_check_prover_hw"
set part "xc7z100ffg900-1"
set wrapped 1
set optLevel   [expr {[lindex $argv 6]}]
set parallelism 64
set elemBitSize 64
set weightelemBitSize 64
set elemInPkt 1
set clk 10
set inputDim   [expr {[lindex $argv 2]}]
set channelNum [expr {[lindex $argv 3]}]
set filterDim  [expr {[lindex $argv 4]}]
set filterNum  [expr {[lindex $argv 5]}]
set batchNum   1 

set filterBurstNum 1

#set logInputDim   0
#set logChannelNum 0
#set logFilterNum  0
#set logFilterDim  0
#set logBatchNum   0
#set logOutputDim  0


proc log_2 num {
    expr { log($num) / [expr log(2)] }
}

proc log_2_ceil num {
    expr int(ceil([log_2 $num]))
}

proc log_2_with_power_of_2_check num {
    set val [log_2 $num]
    if { $val != floor($val) } {
        error "$num is not power of two!!!"
    }
    return [expr int($val)]
}

proc get_output_dim {inputDim filterDim stride} {
    set logOutDim [log_2_ceil [expr {($inputDim-$filterDim)/(1+$stride)}]]
    return [expr 2 ** $logOutDim]
}

set logInputDim [log_2_with_power_of_2_check $inputDim]

set logChannelNum [log_2_with_power_of_2_check $channelNum]

set logFilterNum [log_2_with_power_of_2_check $filterNum]

set logFilterDim [log_2_with_power_of_2_check $filterDim]

set logBatchNum [log_2_with_power_of_2_check $batchNum]

set outputDim [get_output_dim $inputDim $filterDim 1]
set logOutputDim [log_2_with_power_of_2_check $outputDim]


### Script options.
array set opt {
    doCSim                  0
    doSynth                 1
    doCosim                 0
    doExport                0
}
#puts "### Checking script option overrides by environment variables:"
#foreach o [lsort [array names opt]] {
#    if {[info exists env($o)]} {
#        puts "  Setting $o to: [set opt($o) $env($o)]"
#    }
#}

# The Makefile invokes this script from a specific working directory, so we use
# the "current directory" as project directory.
open_project -reset ${hardware}_${version}_${inputDim}_${channelNum}_${filterDim}_${filterNum}_wt_${weightelemBitSize} 

# If "wrapped" is true, we add a "_wrapped" postfix to the top level function.
if {$wrapped == 0} {
    set_top $topFn
} elseif {$wrapped == 1} {
    set_top ${topFn}_wrapped
}

set defines ""

append defines " -DWRAPPED=$wrapped"
append defines " -DELEMENT_BIT_SIZE=$elemBitSize"
append defines " -DWEIGHT_ELEMENT_BIT_SIZE=$weightelemBitSize"
append defines " -DELEMENT_PER_PACKET=$elemInPkt"
append defines " -DINPUT_DIM=$inputDim"
append defines " -DLOG_INPUT_DIM=$logInputDim"
append defines " -DN_CHAN=$channelNum"
append defines " -DLOG_N_CHAN=$logChannelNum"
append defines " -DN_FILT=$filterNum"
append defines " -DLOG_N_FILT=$logFilterNum"
append defines " -DFILTER_DIM=$filterDim"
append defines " -DLOG_FILTER_DIM=$logFilterDim"
append defines " -DN_BATCH=$batchNum"
append defines " -DLOG_N_BATCH=$logBatchNum"
append defines " -DOUTPUT_DIM=$outputDim"
append defines " -DLOG_OUTPUT_DIM=$logOutputDim"
append defines " -DFILTER_BURST_NUM=$filterBurstNum"

add_files     src/$hardware.cpp       -cflags "$defines"
add_files -tb src/${hardware}_test.cpp  -cflags "$defines"

open_solution "solution"

set_part $part
create_clock -period $clk -name default

# Different optimization levels for the different functions.

if {$hardware == "convo" && $topFn == "convo_mi_hw" } {
    if {$optLevel == 0} {
        # TODO!
    } elseif {$optLevel == 2} {

#TODO: set directives for wrapped version!!! This only works for unwrapped
#        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_N_FILT"
#        set_directive_array_partition -type cyclic -factor [expr $channelNum/2] -dim 3 "convo_mi_hw" ac
#        set_directive_array_partition -type cyclic -factor [expr $channelNum/2] -dim 3 "convo_mi_hw" wt
    } elseif {$optLevel == 3} { 
# Constrained memory synthyesis
if {$parallelism < $channelNum} {
# Set directives when parallelism != Channel 
        set_directive_unroll -factor [expr $parallelism] "convo_mi_hw/LOOP_CHAN"
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_CHAN"
        set_directive_array_partition -type complete "convo_mi_hw" sum
        set_directive_array_partition -type cyclic -factor [expr $parallelism] -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type cyclic -factor [expr $parallelism] -dim 3 "convo_mi_hw" wt
} else {
# Set directives when parallelism = Channel, L1 and L2
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_N_FILT"
        set_directive_array_partition -type complete "convo_mi_hw" sum
        set_directive_array_partition -type complete -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type complete -dim 3 "convo_mi_hw" wt
}
# Directives for wrapper
       # set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_IN_AC_ST"
       # set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_IN_WT_ST"
       # #set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_IN_WT_N_CHAN"
       # #set_directive_unroll -factor 16 "convo_mi_hw_wrapped/LOOP_IN_WT_N_CHAN"
       # set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_OUT_OP_ST"
    } elseif {$optLevel == 1} {
# Unconstrained memory model
if {$parallelism < $channelNum} {
# Set directives when parallelism != Channel 
        set_directive_unroll -factor [expr $parallelism] "convo_mi_hw/LOOP_CHAN"
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_CHAN"
        #set_directive_array_partition -type cyclic -factor 64 -dim 1 "convo_mi_hw" sum
        set_directive_array_partition -type complete "convo_mi_hw" sum
        set_directive_array_partition -type cyclic -factor [expr $parallelism]   -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type cyclic -factor [expr $parallelism]   -dim 3 "convo_mi_hw" wt
} else {
# Set directives when parallelism = Channel, L1 and L2
        set_directive_pipeline -II 1 "convo_mi_hw/LOOP_N_FILT"
        set_directive_array_partition -type complete "convo_mi_hw" sum
        set_directive_array_partition -type complete -dim 3 "convo_mi_hw" ac
        set_directive_array_partition -type complete -dim 3 "convo_mi_hw" wt
}
# Directives for wrapper
        set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_IN_AC_ST"
        set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_IN_WT_ST"
        set_directive_pipeline -II 1 "convo_mi_hw_wrapped/LOOP_OUT_OP_ST"
    }
} elseif {$hardware == "convo" && $topFn == "convo_mi_mod_hw"} {
    if {$optLevel == 0} {
        # No Optimization!
    } elseif {$optLevel == 1} {
# Set directives for wrapped version!!! This only works for unwrapped
        set_directive_unroll -factor [expr $parallelism] "convo_mi_mod_hw/LOOP_CHAN"
        set_directive_pipeline -II 1 "convo_mi_mod_hw/LOOP_CHAN"
        set_directive_array_partition -type complete "convo_mi_mod_hw" sum
        set_directive_array_partition -type cyclic -factor [expr $parallelism] -dim 3 "convo_mi_mod_hw" ac
        set_directive_array_partition -type cyclic -factor [expr $parallelism] -dim 3 "convo_mi_mod_hw" wt
    } elseif {$optLevel == 2} {
        # TODO!
    }
} elseif {$hardware == "verified_convo" && $topFn == "convo_sum_check_prover_hw"} {
    if {$optLevel == 0} {
        # No Optimization!
    } elseif {$optLevel == 1} {
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var1_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var2_u"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var3_t"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var4_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var5_x"
        #set_directive_pipeline -II 1 "update_sum/UPDATE_SUM_1_1_1"
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" temp
        set_directive_unroll -factor 2 "convo_sum_check_prover_hw/INIT_FILTERS_Kernels"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/INIT_FILTERS_Kernels"
        set_directive_array_partition -type block -factor 10 -dim 1 "convo_sum_check_prover_hw" activation_tensor
    } elseif {$optLevel == 2} {
        set_directive_unroll -factor 4 "convo_sum_check_prover_hw/R_var1_y"
        set_directive_pipeline -II 1 "convo_sum_check_prover_hw/R_var1_v"
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" filters
        #set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" activation_tensor
        set_directive_array_partition -type cyclic -factor 32 -dim 1 "convo_sum_check_prover_hw" sum_1
        #set_directive_array_partition -type complete "convo_sum_check_prover_hw" sum_2
        set_directive_array_partition -type complete "convo_sum_check_prover_hw" temp
    }
}




#if {$opt(doCSim)} {
#    puts "\n"
#    puts "************************************************************************"
#    puts "* Running csim_design to validate C code is functional"
#    puts "************************************************************************"
#    puts "\n"
#    csim_design -clean
#}
#
#if {$opt(doSynth)} {
#    puts "\n"
#    puts "************************************************************************"
#    puts "*  Running csynth_design to create core RTL"
#    puts "************************************************************************"
#    puts "\n"
csynth_design
#}
#
#if {$opt(doCosim)} {
#    puts "\n"
#    puts "************************************************************************"
#    puts "*  Running cosim_design to verify RTL"
#    puts "************************************************************************"
#    puts "\n"
#    cosim_design
#}
#
#if {$opt(doExport)} {
#    puts "\n"
#    puts "*************************************************************************"
#    puts "*  Running export_design to generate IP package"
#    puts "*************************************************************************"
#    puts "\n"
#    export_design
#}

close_project
exit
