#!/usr/bin/env python

from itertools import product

### This is the definition of all the different synthesis configurations.

OPTIONS = {
    # 'mmult' is the hardware name, selecting the .cpp file (e.g. mmult.cpp) to be used.
    'mmult' : {
        'part': ['zynq'],  # E.g., zynq zynquplus xc7z020clg484-1 xc7z020clg484-2
        'clock': [10],  # Clock period.
        'topFn': ['mmult_hw'],  # The top level function to synthesize.
        'wrapped': [0],  # If wrapped=1, the top level function gets a '_wrapped' postfix.
        'optLevel': [2], # 0 1 # Different optimization options defined in run_hls.tcl.
        'elemInPkt': [1, 2],  # How many elements to transfer at a time on the bus.
        'elemBitSize': [64],  # Element type is always unsigned int. This selects bit size.
        'inputDim': [32], # 64 # Dimension of the input (e.g., 32 means 32x32 matrix).
    },
    'convo' : {
        'part': ['xc7z100ffg900-1'],
        'clock': [10],
        'topFn': ['convo_mi_hw'], #, 'convo_ref_hw'],
        #'topFn': ['convo_mi_mod_hw'], #, 'convo_ref_hw'],
        'wrapped': [0],
        'optLevel': [1], #base: Opt1, can get D parallelism
        'parallelism': [64],
        'elemInPkt':  [1],
        'elemBitSize': [64],
        'weightelemBitSize': [64],  
        'inputDim': [32],
        'channelNum': [64],
        'filterNum': [256],
        'filterDim': [8],
        'filterBurstNum': [1],
        'batchNum': [1],
    },
    'verified_convo' : {
        'part': ['xc7z100ffg900-1'],
        'clock': [10],
        'topFn': ['convo_sum_check_prover_quad_hw'],#['convo_sum_check_prover_small_hw'],
        #'topFn': ['convo_sum_check_prover_hw'],
        'wrapped': [0],
        'optLevel': [1], # base: 1
        'parallelism': [64], #not used
        'elemInPkt': [1],
        'elemBitSize': [64],
        'weightelemBitSize': [64],  
        'inputDim': [16],
        'channelNum': [256],
        'filterNum': [1],
        'filterDim': [4],
        'filterBurstNum': [1],
        'batchNum': [1],
    },
    'verified_quad' : {
        'part': ['xc7z100ffg900-1'],
        'clock': [10],
        'topFn': ['convo_sum_check_prover_quad_hw'],#['convo_sum_check_prover_small_hw'],
        'wrapped': [0],
        'optLevel': [1], # base: 1
        'parallelism': [64], #not used
        'elemInPkt': [1],
        'elemBitSize': [64],
        'weightelemBitSize': [64],  
        'inputDim': [64],
        'channelNum': [4],
        'filterNum': [1],
        'filterDim': [8],
        'filterBurstNum': [1],
        'batchNum': [1],
    },
}

### Utility functions used elsewhere.

# Returns the different configurations of source name by taking the cross product of options.
def get_cross_product(options):
    keys = options.keys()
    vals = options.values()
    return [dict(zip(keys, config)) for config in product(*vals)]

# Constructs output directory name from hardware (e.g. mmult, convo) and config dict.
def config_to_dirname(hardware, config):
    dirname = hardware
    for opt in sorted(config):
        dirname += '+' + opt + '_' + str(config[opt])
    return dirname

# Returns a dictionary mapping:
#   Hardware name -> Config directory name -> Configuration dictionary object.
def get_hardware_to_dirname_to_config_map():
    hardware_to_dirname_to_config = {}
    # Go through hardware names (e.g., 'mmult', 'convo', etc..).
    for hardware in OPTIONS:
        hardware_to_dirname_to_config[hardware] = {}
        dirname_to_config = hardware_to_dirname_to_config[hardware]
        # Go through all configuration.
        for config in get_cross_product(OPTIONS[hardware]):
            dirname = config_to_dirname(hardware, config)
            # Save the hardware too in the config object.
            config['hardware'] = hardware
            dirname_to_config[dirname] = config
    return hardware_to_dirname_to_config

if __name__== "__main__":
    hardware_to_dirname_to_config = get_hardware_to_dirname_to_config_map()
    for hardware in sorted(hardware_to_dirname_to_config):
        print "### Configs for hardware:", hardware
        print
        configdir_names = hardware_to_dirname_to_config[hardware]
        for dirname in sorted(configdir_names):
            print dirname + ':', configdir_names[dirname]
            print
