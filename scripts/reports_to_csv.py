#!/usr/bin/env python

# Takes hardware name (e.g., mmult, convo, prover) as command line argument,
# collects the report of all configurations (defined in config.py), and creates a CSV file,
# e.g.: csvs/mmult-results.csv.

import os
import sys
import glob
import xmltodict
import csv

import config

def print_summary(hardware):
    hardware_to_dirname_to_config = config.get_hardware_to_dirname_to_config_map()
    dirname_to_config = hardware_to_dirname_to_config[hardware]
    print
    for dirname in dirname_to_config:
        print dirname
        report_path_pattern = os.path.join('results', dirname, 'solution/syn/report/*.xml')
        report_path = glob.glob(report_path_pattern)[0]
        print
        with open(report_path) as report_file:
            report = xmltodict.parse(report_file.read())

            profile = report['profile']
            latency_summary = profile['PerformanceEstimates']['SummaryOfOverallLatency']
            latency = latency_summary['Average-caseLatency']
            print '%10s %s' % (latency, "cycles"),

            resources = profile['AreaEstimates']['Resources']
            for resource in sorted(resources):
                print '%10s %s' % (resources[resource], resource),

        print
        print


output_columns = ['Cycles', 'BRAM_18K', 'DSP48E', 'FF', 'LUT']

def write_csv(hardware):
    print 'Collecting report data into CSV file: csv/' + hardware + '-results.csv'

    # Get the config map.
    hardware_to_dirname_to_config = config.get_hardware_to_dirname_to_config_map()
    dirname_to_config = hardware_to_dirname_to_config[hardware]
    config_options_columns = dirname_to_config.values()[0].keys()

    # Initialize the output CSV.
    csv_path = os.path.join('csvs', hardware + '-results.csv')
    with open(csv_path, mode='w') as csv_file:

        columns = ['ConfigName']
        columns += config_options_columns
        columns += output_columns

        writer = csv.DictWriter(csv_file, fieldnames=columns)
        writer.writeheader()

        for dirname in dirname_to_config:
            # Create dictionary for the row, and add input columns.
            row = {}
            row['ConfigName'] = dirname

            # Add config options.
            c = dirname_to_config[dirname]
            for option_col in config_options_columns:
                row[option_col] = c[option_col]

            # Get the report xml path.
            report_name = c['topFn']
            if c['wrapped']:
                report_name = report_name + '_wrapped'
            report_path = os.path.join('results', dirname,
                                       'solution/syn/report/' + report_name + '_csynth.xml')

            if not os.path.isfile(report_path):
                for output_col in output_columns:
                    row[output_col] = 'n/a'
            else:
                with open(report_path) as report_file:
                    # Read in report.
                    report = xmltodict.parse(report_file.read())

                    # Add latency.
                    profile = report['profile']
                    latency_summary = profile['PerformanceEstimates']['SummaryOfOverallLatency']
                    latency = latency_summary['Average-caseLatency']
                    row['Cycles'] = latency

                    # Add resources.
                    resources = profile['AreaEstimates']['Resources']
                    for resource in resources:
                        row[resource] = resources[resource]

            # Write the row out.
            writer.writerow(row)


hardware_name = sys.argv[1]
# print_summary(hardware_name)
write_csv(hardware_name)
