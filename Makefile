ifndef XILINX_VIVADO
$(error "XILINX_VIVADO is not defined! \
  You need to set up the vivado environment, e.g., by running \
  /opt/Xilinx/Vivado/2016.2/settings64.sh")
endif

######################################################################
### Common variables.
HLS               := vivado_hls

CC                := g++
COPT              := -I$(XILINX_VIVADO)/include/ \
                     -I$(subst Vivado,Vivado_HLS,$(XILINX_VIVADO))/include/

SRC_DIR           := src

TEST_BIN_DIR      := bin
SYNTH_DIR         := results
CSV_DIR           := csvs

######################################################################
### Python virtual environment.

VIRT_ENV_DIR := venv
PYTHON       := $(VIRT_ENV_DIR)/bin/python

$(VIRT_ENV_DIR)/bin/activate: requirements.txt
	test -d $(VIRT_ENV_DIR) || virtualenv $(VIRT_ENV_DIR)
	$(VIRT_ENV_DIR)/bin/pip install --upgrade --requirement requirements.txt
	touch $(VIRT_ENV_DIR)/bin/activate

python-environment: $(VIRT_ENV_DIR)/bin/activate

clean-python-environment:
	rm -r $(VIRT_ENV_DIR)

######################################################################
### Output directory rules.

$(TEST_BIN_DIR) $(CSV_DIR):
	mkdir -p $@

clean:
	rm -r $(TEST_BIN_DIR) $(SYNTH_DIR) $(CSV_DIR) Makefile.rules

######################################################################
### Matrix multiplication quick C tests.

MMULT_SRCS := $(SRC_DIR)/mmult_test.cpp $(SRC_DIR)/mmult.cpp $(SRC_DIR)/mmult.h

$(TEST_BIN_DIR)/mmult_standalone_test: WRAPPED = 0
$(TEST_BIN_DIR)/mmult_wrapped_test:    WRAPPED = 1

$(TEST_BIN_DIR)/mmult_standalone_test \
$(TEST_BIN_DIR)/mmult_wrapped_test: $(MMULT_SRCS) | $(TEST_BIN_DIR)
	@$(CC) $(COPT) \
    -DWRAPPED=$(WRAPPED) \
    -DINPUT_DIM=16 \
    -DELEMENT_BIT_SIZE=64 \
    -DELEMENT_PER_PACKET=2 \
    $^ -o $@

# test-mmult-standalone: $(TEST_BIN_DIR)/mmult_standalone_test
# 	@$<

# test-mmult-wrapped: $(TEST_BIN_DIR)/mmult_wrapped_test
# 	@$<

# test-mmult: test-mmult-standalone test-mmult-wrapped
test-mmult: $(TEST_BIN_DIR)/mmult_wrapped_test
	@$<

######################################################################
### Convolution C tests.

CONVO_SRCS := $(SRC_DIR)/convo_test.cpp $(SRC_DIR)/convo.cpp $(SRC_DIR)/convo.h

$(TEST_BIN_DIR)/convo_standalone_test: WRAPPED = 0
$(TEST_BIN_DIR)/convo_wrapped_test:    WRAPPED = 1

$(TEST_BIN_DIR)/convo_standalone_test \
$(TEST_BIN_DIR)/convo_wrapped_test: $(CONVO_SRCS) | $(TEST_BIN_DIR)
	@$(CC) $(COPT) -DWRAPPED=$(WRAPPED) -DQUICK_TEST $^ -o $@

test-convo-standalone: $(TEST_BIN_DIR)/convo_standalone_test
	@$<

test-convo-wrapped: $(TEST_BIN_DIR)/convo_wrapped_test
	@$<

test-convo: test-convo-standalone test-convo-wrapped
# test-convo: $(TEST_BIN_DIR)/convo_standalone_test
# 	@$<

######################################################################
### Verified convolution C tests.

VERIFIED_CONVO_SRCS := $(SRC_DIR)/verified_convo_test.cpp $(SRC_DIR)/verified_convo.cpp $(SRC_DIR)/verified_convo.h

$(TEST_BIN_DIR)/verified_convo_standalone_test: WRAPPED = 0
$(TEST_BIN_DIR)/verified_convo_wrapped_test:    WRAPPED = 1

$(TEST_BIN_DIR)/verified_convo_standalone_test \
$(TEST_BIN_DIR)/verified_convo_wrapped_test: \
 $(VERIFIED_CONVO_SRCS) | $(TEST_BIN_DIR)
	@$(CC) $(COPT) -DWRAPPED=$(WRAPPED) -DQUICK_TEST $^ -o $@

# test-verified_convo-standalone: $(TEST_BIN_DIR)/verified_convo_standalone_test
# 	@$<

# test-verified_convo-wrapped: $(TEST_BIN_DIR)/verified_convo_wrapped_test
# 	@$<

# test-verified_convo: test-verified_convo-standalone test-verified_convo-wrapped
test-verified_convo: $(TEST_BIN_DIR)/verified_convo_standalone_test
	@$<

######################################################################
### Verified quadratic activation C tests.

VERIFIED_QUAD_SRCS := \
  $(SRC_DIR)/verified_quad_test.cpp \
  $(SRC_DIR)/verified_quad.cpp \
  $(SRC_DIR)/verified_quad.h

$(TEST_BIN_DIR)/verified_quad_test: $(VERIFIED_QUAD_SRCS) | $(TEST_BIN_DIR)
	$(CC) $(COPT) -O3 -DQUICK_TEST $^ -o $@

test-verified_quad: $(TEST_BIN_DIR)/verified_quad_test
	/usr/bin/time -p $<

######################################################################
### Synthesis rules are generated.

Makefile.rules: scripts/generate_makefile_rules.py scripts/config.py
	./$< > $@

-include Makefile.rules
